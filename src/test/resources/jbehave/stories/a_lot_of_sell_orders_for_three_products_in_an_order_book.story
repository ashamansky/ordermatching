Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario 50: a lot of sell orders for three products in an order books

When brokers add new orders with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|1|1|ORDER|ABC|SELL|15|10|B001|C001|
|2|2|ORDER|ABCD|SELL|150|100|B001|C001|
|3|3|ORDER|XYZ|SELL|30|20|B001|C001|
|4|4|ORDER|ABC|SELL|16|11|B002|C002|
|5|5|ORDER|ABCD|SELL|160|110|B002|C002|
|6|6|ORDER|XYZ|SELL|32|22|B002|C002|
|7|7|ORDER|ABC|SELL|17|9|B001|C003|
|8|8|ORDER|ABCD|SELL|170|90|B001|C003|
|9|9|ORDER|XYZ|SELL|34|18|B001|C003|
|10|10|ORDER|ABC|SELL|10|5|B001|C004|
|11|11|ORDER|ABCD|SELL|100|50|B001|C004|
|12|12|ORDER|XYZ|SELL|20|10|B001|C004|
|13|13|ORDER|ABC|SELL|10|10|B003|C005|
|14|14|ORDER|ABCD|SELL|100|100|B003|C005|
|15|15|ORDER|XYZ|SELL|20|20|B003|C005|
|16|16|ORDER|ABC|SELL|5|11|B001|C001|
|17|17|ORDER|ABCD|SELL|50|110|B001|C001|
|18|18|ORDER|XYZ|SELL|10|22|B001|C001|
|19|19|ORDER|ABC|SELL|15|3|B001|C003|
|20|20|ORDER|ABCD|SELL|150|30|B001|C003|
|21|21|ORDER|XYZ|SELL|30|6|B001|C003|
|22|22|ORDER|ABC|SELL|15|12|B004|C006|
|23|23|ORDER|ABCD|SELL|150|120|B004|C006|
|24|24|ORDER|XYZ|SELL|30|24|B004|C006|
|25|25|ORDER|ABC|SELL|20|10|B001|C001|
|26|26|ORDER|ABCD|SELL|200|100|B001|C001|
|27|27|ORDER|XYZ|SELL|40|20|B001|C001|

Then buy entries in the order book for product ABC is empty

And buy entries in the order book for product ABCD is empty

And buy entries in the order book for product XYZ is empty

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|19|19|15|3|B001|C003|
|10|10|10|5|B001|C004|
|7|7|17|9|B001|C003|
|1|1|15|10|B001|C001|
|13|13|10|10|B003|C005|
|25|25|20|10|B001|C001|
|4|4|16|11|B002|C002|
|16|16|5|11|B001|C001|
|22|22|15|12|B004|C006|

And sell entries in the order book for product ABCD contains:
|order_id|timestamp|amount|price|broker|client|
|20|20|150|30|B001|C003|
|11|11|100|50|B001|C004|
|8|8|170|90|B001|C003|
|2|2|150|100|B001|C001|
|14|14|100|100|B003|C005|
|26|26|200|100|B001|C001|
|5|5|160|110|B002|C002|
|17|17|50|110|B001|C001|
|23|23|150|120|B004|C006|

And sell entries in the order book for product XYZ contains:
|order_id|timestamp|amount|price|broker|client|
|21|21|30|6|B001|C003|
|12|12|20|10|B001|C004|
|9|9|34|18|B001|C003|
|3|3|30|20|B001|C001|
|15|15|20|20|B003|C005|
|27|27|40|20|B001|C001|
|6|6|32|22|B002|C002|
|18|18|10|22|B001|C001|
|24|24|30|24|B004|C006|

And transaction list is empty
