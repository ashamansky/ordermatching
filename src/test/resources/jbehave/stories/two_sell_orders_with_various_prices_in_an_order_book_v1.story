Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: two sell orders with various prices (variant 1) in an order book

Given sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|2|2|ORDER|ABC|SELL|10|8|B002|C002|

Then buy entries in the order book for product ABC is empty

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|2|2|10|8|B002|C002|
|1|1|10|10|B001|C001|

And transaction list is empty
