Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: a lot of sell orders in an order book

When brokers add new orders with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|1|1|ORDER|ABC|SELL|15|10|B001|C001|
|2|2|ORDER|ABC|SELL|16|11|B002|C002|
|3|3|ORDER|ABC|SELL|17|9|B001|C003|
|4|4|ORDER|ABC|SELL|10|5|B001|C004|
|5|5|ORDER|ABC|SELL|10|10|B003|C005|
|6|6|ORDER|ABC|SELL|5|11|B001|C001|
|7|7|ORDER|ABC|SELL|15|3|B001|C003|
|8|8|ORDER|ABC|SELL|15|12|B004|C006|
|9|9|ORDER|ABC|SELL|20|10|B001|C001|

Then buy entries in the order book for product ABC is empty

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|7|7|15|3|B001|C003|
|4|4|10|5|B001|C004|
|3|3|17|9|B001|C003|
|1|1|15|10|B001|C001|
|5|5|10|10|B003|C005|
|9|9|20|10|B001|C001|
|2|2|16|11|B002|C002|
|6|6|5|11|B001|C001|
|8|8|15|12|B004|C006|

And transaction list is empty
