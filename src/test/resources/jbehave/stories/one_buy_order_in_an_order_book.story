Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: one buy order in an order book

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|1|1|ORDER|ABC|BUY|10|10|B001|C001|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|

And sell entries in the order book for product ABC is empty

And transaction list is empty
