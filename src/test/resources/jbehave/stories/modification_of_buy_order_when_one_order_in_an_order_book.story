Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario 31: modification of buy order (one order in an order book)

Given buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|

When a broker add new order with parameters:
|order_id|timestamp|type|modifiedOrderId|amount|price|broker|
|2|2|MODIFICATION|1|11|16|B001|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|2|11|16|B001|C001|

And sell entries in the order book for product ABC is empty

And transaction list is empty