Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: three buy orders in an order book

Given buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|
|2|2|10|9|B002|C002|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|3|3|ORDER|ABC|BUY|15|10|B001|C003|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|
|3|3|15|10|B001|C003|
|2|2|10|9|B002|C002|

And sell entries in the order book for product ABC is empty

And transaction list is empty
