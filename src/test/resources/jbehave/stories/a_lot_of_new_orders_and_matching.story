Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: a lot of new orders and matching

Given buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|12|15|B001|C001|
|4|4|8|14|B001|C003|

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|3|3|10|17|B002|C002|
|2|2|10|18|B002|C004|
|5|5|5|19|B003|C004|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|6|6|ORDER|ABC|BUY|10|15|B001|C002|
|7|7|ORDER|ABC|BUY|10|13|B003|C002|
|8|8|ORDER|ABC|BUY|12|14|B002|C003|
|9|9|ORDER|ABC|SELL|35|14|B002|C004|
|10|10|ORDER|ABC|BUY|8|16|B004|C002|
|11|11|ORDER|ABC|BUY|5|17|B005|C002|
|12|12|ORDER|ABC|SELL|10|20|B006|C003|
|13|13|ORDER|ABC|SELL|15|21|B006|C004|
|14|14|ORDER|ABC|SELL|10|15|B002|C003|
|15|15|ORDER|ABC|SELL|9|15|B001|C002|
|16|16|ORDER|ABC|SELL|7|15|B002|C004|
|17|17|ORDER|ABC|BUY|10|14|B002|C005|
|18|18|ORDER|ABC|BUY|12|15|B001|C003|
|19|19|ORDER|ABC|BUY|20|19|B006|C003|
|20|20|ORDER|ABC|BUY|20|20|B002|C002|
|21|21|ORDER|ABC|BUY|30|25|B002|C004|
|22|22|ORDER|ABC|SELL|40|17|B001|C002|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|8|8|7|14|B002|C003|
|17|17|10|14|B002|C005|
|7|7|10|13|B003|C002|

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|22|22|21|17|B001|C002|

And transaction list contains:
|transaction_id|product|amount|price|brokerBuy|brokerSell|clientBuy|clientSell|
|1|ABC|12|15|B001|B002|C001|C004|
|2|ABC|10|15|B001|B002|C002|C004|
|3|ABC|8|14|B001|B002|C003|C004|
|4|ABC|5|14|B002|B002|C003|C004|
|5|ABC|5|17|B005|B002|C002|C002|
|6|ABC|8|16|B004|B002|C002|C003|
|7|ABC|2|15|B001|B002|C003|C003|
|8|ABC|9|15|B001|B001|C003|C002|
|9|ABC|1|15|B001|B002|C003|C004|
|10|ABC|6|15|B006|B002|C003|C004|
|11|ABC|5|17|B006|B002|C003|C002|
|12|ABC|9|18|B006|B002|C003|C004|
|13|ABC|1|18|B002|B002|C002|C004|
|14|ABC|5|19|B002|B003|C002|C004|
|15|ABC|10|20|B002|B006|C002|C003|
|16|ABC|15|21|B002|B006|C004|C004|
|17|ABC|15|25|B002|B001|C004|C002|
|18|ABC|4|20|B002|B001|C002|C002|
