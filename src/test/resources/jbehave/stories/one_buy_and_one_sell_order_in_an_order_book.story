Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: one buy order and one sell order in an order book

Given buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|2|2|ORDER|ABC|SELL|10|15|B002|C002|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|2|2|10|15|B002|C002|

And transaction list is empty
