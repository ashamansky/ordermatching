Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario 37: modification (increasing) of amount in sell order (more than one order in an order book)

Given sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|
|3|3|15|10|B001|C003|
|2|2|10|12|B002|C002|

When a broker add new order with parameters:
|order_id|timestamp|type|modifiedOrderId|amount|price|broker|
|4|4|MODIFICATION|1|14|10|B001|

Then buy entries in the order book for product ABC is empty

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|3|3|15|10|B001|C003|
|1|4|14|10|B001|C001|
|2|2|10|12|B002|C002|

And transaction list is empty