Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: full partial match with various prices (variant 1)

Given sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|
|2|2|10|11|B001|C003|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|3|3|ORDER|ABC|BUY|14|15|B002|C002|

Then buy entries in the order book for product ABC is empty

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|2|2|6|11|B001|C003|

And transaction list contains:
|transaction_id|product|amount|price|brokerBuy|brokerSell|clientBuy|clientSell|
|1|ABC|10|10|B002|B001|C002|C001|
|2|ABC|4|11|B002|B001|C002|C003|
