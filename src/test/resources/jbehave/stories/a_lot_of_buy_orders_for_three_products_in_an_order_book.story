Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario 49: a lot of buy orders for three products in an order books

When brokers add new orders with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|1|1|ORDER|ABC|BUY|15|10|B001|C001|
|2|2|ORDER|ABCD|BUY|150|100|B001|C001|
|3|3|ORDER|XYZ|BUY|30|20|B001|C001|
|4|4|ORDER|ABC|BUY|16|9|B002|C002|
|5|5|ORDER|XYZ|BUY|32|18|B002|C002|
|6|6|ORDER|ABC|BUY|17|11|B001|C003|
|7|7|ORDER|ABCD|BUY|160|90|B002|C002|
|8|8|ORDER|ABCD|BUY|170|110|B001|C003|
|9|9|ORDER|XYZ|BUY|34|22|B001|C003|
|10|10|ORDER|ABC|BUY|10|15|B001|C004|
|11|11|ORDER|ABCD|BUY|100|150|B001|C004|
|12|12|ORDER|XYZ|BUY|20|30|B001|C004|
|13|13|ORDER|ABC|BUY|10|10|B003|C005|
|14|14|ORDER|XYZ|BUY|20|20|B003|C005|
|15|15|ORDER|ABC|BUY|5|9|B001|C001|
|16|16|ORDER|ABC|BUY|15|20|B001|C003|
|17|17|ORDER|ABCD|BUY|100|100|B003|C005|
|18|18|ORDER|XYZ|BUY|10|18|B001|C001|
|19|19|ORDER|ABCD|BUY|50|90|B001|C001|
|20|20|ORDER|ABCD|BUY|150|200|B001|C003|
|21|21|ORDER|XYZ|BUY|30|40|B001|C003|
|22|22|ORDER|ABC|BUY|15|8|B004|C006|
|23|23|ORDER|XYZ|BUY|30|16|B004|C006|
|24|24|ORDER|ABCD|BUY|150|80|B004|C006|
|25|25|ORDER|ABC|BUY|20|10|B001|C001|
|26|26|ORDER|ABCD|BUY|200|100|B001|C001|
|27|27|ORDER|XYZ|BUY|40|20|B001|C001|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|16|16|15|20|B001|C003|
|10|10|10|15|B001|C004|
|6|6|17|11|B001|C003|
|1|1|15|10|B001|C001|
|13|13|10|10|B003|C005|
|25|25|20|10|B001|C001|
|4|4|16|9|B002|C002|
|15|15|5|9|B001|C001|
|22|22|15|8|B004|C006|

And buy entries in the order book for product ABCD contains:
|order_id|timestamp|amount|price|broker|client|
|20|20|150|200|B001|C003|
|11|11|100|150|B001|C004|
|8|8|170|110|B001|C003|
|2|2|150|100|B001|C001|
|17|17|100|100|B003|C005|
|26|26|200|100|B001|C001|
|7|7|160|90|B002|C002|
|19|19|50|90|B001|C001|
|24|24|150|80|B004|C006|

And buy entries in the order book for product XYZ contains:
|order_id|timestamp|amount|price|broker|client|
|21|21|30|40|B001|C003|
|12|12|20|30|B001|C004|
|9|9|34|22|B001|C003|
|3|3|30|20|B001|C001|
|14|14|20|20|B003|C005|
|27|27|40|20|B001|C001|
|5|5|32|18|B002|C002|
|18|18|10|18|B001|C001|
|23|23|30|16|B004|C006|

And sell entries in the order book for product ABC is empty

And sell entries in the order book for product ABCD is empty

And sell entries in the order book for product XYZ is empty

And transaction list is empty

