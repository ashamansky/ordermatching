Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario 39: modification of partial matched buy order

Given buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|
|3|3|15|10|B001|C003|
|2|2|10|9|B002|C002|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|4|4|ORDER|ABC|SELL|4|10|B002|C002|

And a broker add new order with parameters:
|order_id|timestamp|type|modifiedOrderId|amount|price|broker|
|5|5|MODIFICATION|1|20|10|B001|

And a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|6|6|ORDER|ABC|BUY|4|10|B002|C002|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|3|3|15|10|B001|C003|
|1|5|20|10|B001|C001|
|6|6|4|10|B002|C002|
|2|2|10|9|B002|C002|

And sell entries in the order book for product ABC is empty

And transaction list contains:
|transaction_id|product|amount|price|brokerBuy|brokerSell|clientBuy|clientSell|
|1|ABC|4|10|B001|B002|C001|C002|