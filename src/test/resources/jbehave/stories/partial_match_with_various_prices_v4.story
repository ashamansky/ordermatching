Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: partial match with various prices (variant 4)

Given buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|15|B001|C001|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|2|2|ORDER|ABC|SELL|14|10|B002|C002|

Then buy entries in the order book for product ABC is empty

And sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|2|2|4|10|B002|C002|

And transaction list contains:
|transaction_id|product|amount|price|brokerBuy|brokerSell|clientBuy|clientSell|
|1|ABC|10|15|B001|B002|C001|C002|
