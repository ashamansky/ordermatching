Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: full partial match with the same price (variant 2)

Given sell entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|1|1|10|10|B001|C001|
|2|2|4|10|B002|C004|

When a broker add new order with parameters:
|order_id|timestamp|type|product|side|amount|price|broker|client|
|3|3|ORDER|ABC|BUY|15|10|B002|C002|

Then buy entries in the order book for product ABC contains:
|order_id|timestamp|amount|price|broker|client|
|3|3|1|10|B002|C002|

And sell entries in the order book for product ABC is empty

And transaction list contains:
|transaction_id|product|amount|price|brokerBuy|brokerSell|clientBuy|clientSell|
|1|ABC|10|10|B002|B001|C002|C001|
|2|ABC|4|10|B002|B002|C002|C004|
