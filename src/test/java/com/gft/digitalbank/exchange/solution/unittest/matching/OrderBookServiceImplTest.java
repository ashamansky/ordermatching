package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.solution.matching.*;
import com.gft.digitalbank.exchange.solution.matching.impl.OrderBookMapper;
import com.gft.digitalbank.exchange.solution.matching.impl.OrderRegistryImpl;
import com.gft.digitalbank.exchange.solution.matching.sequence.OrderIDCreator;
import com.gft.digitalbank.exchange.solution.repository.OrderRepository;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderRegistryStubs;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.TestableOrderBookServiceImpl;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class OrderBookServiceImplTest {
    private static final String SECURITY_ABC = "ABC";
    private static final String SECURITY_DEF = "DEF";

    @Mock
    private OrderBookMapper mapper;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderIDCreator orderIDCreator;

    @InjectMocks
    private TestableOrderBookServiceImpl orderBookService;


    @BeforeMethod
    public void setUp() {
        this.orderBookService = new TestableOrderBookServiceImpl();
        initMocks(this);
        this.orderBookService.setRegistries(new HashMap<>());
    }

    @Test
    public void getBookCreatesMissingRegistry() {
        when(orderRepository.findOrderBookByProduct(SECURITY_ABC)).thenReturn(newOrderRegistry(SECURITY_ABC));

        assertNotNull(orderBookService.getBook(SECURITY_ABC));
    }

    @Test
    public void getBookReturnsSameObjectForEachCall() {
        OrderRegistry orderRegistry = orderBookService.getBook(SECURITY_ABC);

        assertEquals(orderBookService.getBook(SECURITY_ABC), orderRegistry);
    }

    @Test
    public void getBookReturnsDifferentRegistriesForEachSecurity() {
        when(orderRepository.findOrderBookByProduct(SECURITY_ABC)).thenReturn(newOrderRegistry(SECURITY_ABC));
        when(orderRepository.findOrderBookByProduct(SECURITY_DEF)).thenReturn(newOrderRegistry(SECURITY_DEF));

        assertNotEquals(orderBookService.getBook(SECURITY_ABC), orderBookService.getBook(SECURITY_DEF));
    }


    @Test
    public void createOrderRegistry() {
        when(orderRepository.findOrderBookByProduct(SECURITY_ABC)).thenReturn(newOrderRegistry(SECURITY_ABC));

        OrderRegistry abc = this.orderBookService.getBook(SECURITY_ABC);
        assertNotNull(abc);
        assertEquals(this.orderBookService.getBook(SECURITY_ABC), abc);
    }

    @Test(dataProvider = "multipleSecuritiesRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void findBookByBrokerAndIdReturnsNullWhenNoMatch(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        int orderId = 8;
        String broker = "broker1";
        this.orderBookService.setRegistries(registry);

        OrderRegistry orderRegistry = this.orderBookService.findBookByBrokerAndId(broker, orderId);

        assertNull(orderRegistry);
    }

    private OrderRegistry newOrderRegistry(String security) {
        return new OrderRegistryImpl(security);
    }

}