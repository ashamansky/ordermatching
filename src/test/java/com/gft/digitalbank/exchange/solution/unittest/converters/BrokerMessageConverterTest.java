package com.gft.digitalbank.exchange.solution.unittest.converters;

import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.solution.converters.BrokerMessageConverter;
import com.gft.digitalbank.exchange.solution.exceptions.InvalidBrokerMessageException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class BrokerMessageConverterTest {

    @DataProvider
    public Object[][] validBrokerMessages() {
        return BrokerMessageDataProvider.getValidBrokerMessages();
    }

    @DataProvider
    public Object[][] invalidBrokerMessages() {
        return BrokerMessageDataProvider.getInvalidBrokerMessages();
    }

    @Test(expectedExceptions = InvalidBrokerMessageException.class, dataProvider = "invalidBrokerMessages")
    public void shouldThrowExceptionWhenInvalidBrokerMessage(String jsonFormat) {
        // Given
        BrokerMessageConverter brokerMessageConverter = new BrokerMessageConverter();

        // When
        BrokerMessage actual = brokerMessageConverter.convert(jsonFormat);

        // Then
        assertNull(actual);
    }

    @Test(dataProvider = "validBrokerMessages")
    public void shouldConvertWhenValidBrokerMessage(String jsonFormat) {
        // Given
        BrokerMessageConverter brokerMessageConverter = new BrokerMessageConverter();

        // When
        BrokerMessage actual = brokerMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual);
    }
}