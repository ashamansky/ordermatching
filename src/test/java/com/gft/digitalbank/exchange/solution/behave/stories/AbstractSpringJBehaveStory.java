package com.gft.digitalbank.exchange.solution.behave.stories;

import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.io.*;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.FilePrintStreamFactory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.*;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

/**
 * Created by alexander on 13.07.16.
 */
public abstract class AbstractSpringJBehaveStory extends JUnitStory {

    private static final String STORY_TIMEOUT = "120";

    @Autowired
    private ApplicationContext applicationContext;

    AbstractSpringJBehaveStory() {
        Embedder embedder = new Embedder();
        embedder.useEmbedderControls(embedderControls());
        embedder.useMetaFilters(Arrays.asList("-skip"));
        useEmbedder(embedder);
    }

    private EmbedderControls embedderControls() {
        return new EmbedderControls()
                .doIgnoreFailureInView(true)
                .useStoryTimeouts(STORY_TIMEOUT);
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new SpringStepsFactory(configuration(), applicationContext);
    }

    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration()
                .useStoryPathResolver(storyPathResolver())
                .useStoryLoader(storyLoader())
                .useStoryReporterBuilder(storyReportBuilder())
                .useParameterControls(parameterControls());
    }

    private ParameterControls parameterControls() {
        return new ParameterControls()
                .useDelimiterNamedParameters(true);
    }

    private StoryReporterBuilder storyReportBuilder() {
        return new StoryReporterBuilder()
                .withCodeLocation(CodeLocations.codeLocationFromClass(this.getClass()))
                .withPathResolver(new FilePrintStreamFactory.ResolveToPackagedName())
                .withFailureTrace(true)
                .withDefaultFormats()
                .withFormats(Format.IDE_CONSOLE, Format.TXT, Format.HTML);
    }

    private StoryLoader storyLoader() {
        return new LoadFromClasspath();
    }

    private StoryPathResolver storyPathResolver() {
        return new BehaveStoriesPathResolver();
    }

    private class BehaveStoriesPathResolver extends UnderscoredCamelCaseResolver {
        static final String BEHAVE_STORIES_PATH = "jbehave/stories";

        BehaveStoriesPathResolver() {
            super();
        }

        @Override
        protected String resolveDirectory(Class<? extends Embeddable> embeddableClass) {
            return BEHAVE_STORIES_PATH;
        }
    }
}
