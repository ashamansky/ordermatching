package com.gft.digitalbank.exchange.solution.unittest.listeners;

import com.gft.digitalbank.exchange.solution.events.Event;
import com.gft.digitalbank.exchange.solution.listeners.OrderProcessingListener;
import com.gft.digitalbank.exchange.solution.repository.BrokerRepository;
import com.gft.digitalbank.exchange.solution.routers.EventRouter;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.EventStubber;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class OrderProcessingListenerTest {

    private EventStubber eventStubber;

    @Mock
    EventRouter eventRouter;

    @Mock
    BrokerRepository brokerRepository;

    @InjectMocks
    private OrderProcessingListener orderProcessingListener = new OrderProcessingListener();

    @BeforeMethod
    public void initMocks() {
        this.eventStubber=new EventStubber();
        MockitoAnnotations.initMocks(this);
        orderProcessingListener.init();
    }

    @Test
    public void shouldAddToQueueWhenNotShutdownEvent() {

        Event buyOrder = eventStubber.ofBuyEvent(5,10);
        when(brokerRepository.getBrokerCount()).thenReturn(1);

        orderProcessingListener.handle(buyOrder);

        verify(eventRouter).route(buyOrder);
    }

    @Test
    public void shouldProceedWhenShutdownEvent() {
        Event shutdownEvent = eventStubber.ofShutdownEvent();
        when(brokerRepository.getBrokerCount()).thenReturn(1);

        orderProcessingListener.init();
        orderProcessingListener.handle(shutdownEvent);

        verify(eventRouter).route(shutdownEvent);
    }
}
