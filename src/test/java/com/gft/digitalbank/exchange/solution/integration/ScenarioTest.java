package com.gft.digitalbank.exchange.solution.integration;

import com.gft.digitalbank.exchange.verification.FunctionalTest;
import com.gft.digitalbank.exchange.verification.scenario.Scenario;
import com.gft.digitalbank.exchange.verification.scenario.functional.*;
import com.gft.digitalbank.exchange.verification.test.VerificationTest;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.Timeout;

import java.util.Arrays;


@Category({FunctionalTest.class})
public class ScenarioTest extends VerificationTest {
    @Rule
    public Timeout timeout = Timeout.seconds(60L);

    public ScenarioTest() {
        super(Arrays.asList(new Scenario[]{
                new Scenario01(),
                new Scenario02(),
                new Scenario03(),
                new Scenario04(),
                new Scenario05(),
                new Scenario06(),
                new Scenario07(),
                new Scenario08(),
                new Scenario09(),
                new Scenario10(),
                new Scenario11(),
                new Scenario12(),
                new Scenario13(),
                new Scenario14(),
                new Scenario15(),
                new Scenario16(),
                new Scenario17(),
                new Scenario19(),
                new Scenario20(),
                new Scenario21(),
                new Scenario22(),
                new Scenario23(),
                new Scenario24(),
                new Scenario25(),
                new Scenario26(),
                new Scenario27(),
                new Scenario28(),
                new Scenario29(),
                new Scenario30(),
                new Scenario31(),
                new Scenario32(),
                new Scenario33(),
                new Scenario34(),
                new Scenario35(),
                new Scenario36(),
                new Scenario37(),
                new Scenario38(),
                new Scenario39(),
                new Scenario40(),
                new Scenario41(),
                new Scenario42(),
                new Scenario43(),
                new Scenario45(),
                new Scenario46(),
                new Scenario47(),
                new Scenario48(),
//                new PerformanceScenario01(),
//                new PerformanceScenario01(),
//                new PerformanceScenario01(),
//                new PerformanceScenario01(),
        }));
    }
}
