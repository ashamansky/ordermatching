package com.gft.digitalbank.exchange.solution.unittest.matching.stubs;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;

public class OrderEntryStubber {
    private int id;
    private long timestamp;
    private int broker;
    private int client;

    public OrderEntryStubber() {
        reset();
    }

    public OrderEntryStubber reset() {
        id = 1;
        timestamp = 1000L;
        client = 1;
        broker = 1;
        return this;
    }

    public OrderEntry of(int amount, int price) {
        String broker = String.format("broker%d", this.broker);
        String client = String.format("client%d", this.client);
        return OrderEntry.builder()
                .id(id)
                .price(price)
                .amount(amount)
                .broker(broker)
                .client(client)
                .timestamp(timestamp)
                .build();
    }

    public OrderEntryStubber inc() {
        ++id;
        ++timestamp;
        ++client;
        ++broker;
        return this;
    }
}
