package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.matching.TransactionMapper;
import com.gft.digitalbank.exchange.solution.matching.impl.TransactionMapperImpl;
import com.gft.digitalbank.exchange.solution.models.LogEntry;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class TransactionMapperImplTest {

    private TransactionMapper transactionMapper;

    @BeforeMethod
    public void setUp() {
        this.transactionMapper = new TransactionMapperImpl();
    }

    @DataProvider
    public static Object[][] logEntriesExamplesProvider() {
        return new Object[][]{
                {
                        OrderEntry.builder().id(1).amount(10).price(15).client("c1").timestamp(1000L).broker("b1").build(),
                        OrderEntry.builder().id(2).amount(10).price(15).client("c6").timestamp(1111L).broker("b7").build(),
                        Transaction.builder().product("ABC").id(1).amount(10).price(15).clientBuy("c1").clientSell("c6").brokerBuy("b1").brokerSell("b7").build()
                },
                {
                        OrderEntry.builder().id(3).amount(200).price(9000).client("c8").timestamp(1232L).broker("b4").build(),
                        OrderEntry.builder().id(4).amount(200).price(9000).client("c3").timestamp(4961L).broker("b4").build(),
                        Transaction.builder().product("DEF").id(2).amount(200).price(9000).clientBuy("c8").clientSell("c3").brokerBuy("b4").brokerSell("b4").build()
                }
        };
    }

    @DataProvider
    public static Object[][] mismatchedAmountExamplesProvider() {
        return new Object[][]{
                {
                        OrderEntry.builder().id(1).amount(1).price(15).client("c1").timestamp(1000L).broker("b1").build(),
                        OrderEntry.builder().id(2).amount(10).price(15).client("c6").timestamp(1111L).broker("b7").build(),
                        Transaction.builder().product("ABC").id(3).amount(1).price(15).clientBuy("c1").clientSell("c6").brokerBuy("b1").brokerSell("b7").build()
                },
                {
                        OrderEntry.builder().id(3).amount(300).price(9000).client("c8").timestamp(1232L).broker("b4").build(),
                        OrderEntry.builder().id(4).amount(250).price(9000).client("c3").timestamp(4961L).broker("b4").build(),
                        Transaction.builder().product("DEF").id(4).amount(250).price(9000).clientBuy("c8").clientSell("c3").brokerBuy("b4").brokerSell("b4").build()
                }
        };
    }

    @Test(dataProvider = "logEntriesExamplesProvider")
    public void fieldsAreCarriedOver(OrderEntry buyEntry, OrderEntry sellEntry, Transaction expectedTransaction) {
        LogEntry logEntry = new LogEntry(expectedTransaction.getId(), expectedTransaction.getProduct(), buyEntry, sellEntry);
        assertEquals(transactionMapper.createTransaction(logEntry), expectedTransaction);
    }

    @Test(dataProvider = "mismatchedAmountExamplesProvider")
    public void transactionAmountIsAmountSold(OrderEntry buyEntry, OrderEntry sellEntry, Transaction expectedTransaction) {
        LogEntry logEntry = new LogEntry(expectedTransaction.getId(), expectedTransaction.getProduct(), buyEntry, sellEntry);
        assertEquals(transactionMapper.createTransaction(logEntry), expectedTransaction);
    }
}