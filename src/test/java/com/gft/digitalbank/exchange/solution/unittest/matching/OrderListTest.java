package com.gft.digitalbank.exchange.solution.unittest.matching;


import com.gft.digitalbank.exchange.solution.matching.OrderList;
import com.gft.digitalbank.exchange.solution.matching.PartitioningOrderList;
import com.gft.digitalbank.exchange.solution.matching.impl.HashMultimapOrderList;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderEntryStubber;
import com.google.common.collect.Sets;
import lombok.val;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;


public class OrderListTest {

    private Random random = new Random(256); // Constant seed for reproducibility
    private Class<? extends OrderList> implementationClass;
    private OrderList orderList;
    private List<OrderEntry> stubs;

    @Factory(dataProvider="testCaseProvider")
    public OrderListTest(Class<? extends OrderList> implementation, OrderEntry[] stubs) {
        this.stubs = Arrays.asList(stubs);
        this.implementationClass = implementation;
    }

    @DataProvider
    public static Object[][] testCaseProvider() {
        final OrderEntry[][] stubs = {
                {},
                {OrderEntry.builder().id(1).price(1011).broker("b1").client("c1").timestamp(1L).build()},
                {
                        OrderEntry.builder().id(1).price(1011).timestamp(1L).client("c1").broker("b1").build(),
                        OrderEntry.builder().id(2).price(1012).timestamp(2L).client("c2").broker("b2").build(),
                        OrderEntry.builder().id(3).price(1013).timestamp(3L).client("c3").broker("b3").build()
                },
                {
                        OrderEntry.builder().id(2).price(1012).timestamp(2L).client("c1").broker("b1").build(),
                        OrderEntry.builder().id(1).price(1011).timestamp(1L).client("c2").broker("b2").build(),
                        OrderEntry.builder().id(3).price(1013).timestamp(3L).client("c3").broker("b3").build()
                }
        };

        ArrayList<Object[]> testCases = new ArrayList<>();
        for(OrderEntry[] stub : stubs) {
            testCases.add(new Object[]{HashMultimapOrderList.class, stub});
        }
        for(OrderEntry[] stub : stubs) {
            testCases.add(new Object[]{PartitioningOrderList.class, stub});
        }
        return testCases.toArray(new Object[stubs.length][]);
    }

    @BeforeMethod
    public void setUp() {
        this.orderList = createListInstance();
        this.stubs.forEach(orderList::add);
    }

    protected OrderList createListInstance() {
        try {
            Constructor<? extends OrderList> constructor = implementationClass.getConstructor();
            OrderList orderList = constructor.newInstance();
            return orderList;
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            Assert.fail(e.getMessage());
        }
        return null;
    }

    @Test
    public void entries() {
        val listEntries = Sets.newHashSet(orderList.entries());
        val stubEntries = Sets.newHashSet(stubs);
        assertEquals(listEntries, stubEntries);
    }

    @Test
    public void add() {
        OrderEntry order = OrderEntry.builder()
                .id(random.nextInt()).price(random.nextInt()).timestamp(random.nextLong()).build();

        orderList.add(order);

        assertTrue(orderList.entries().contains(order));
    }

    @Test
    public void remove() {
        OrderEntry order = OrderEntry.builder()
                .id(random.nextInt()).price(random.nextInt()).timestamp(random.nextLong()).broker("b1").client("c1").build();

        OrderEntry same = order.toBuilder().build();

        orderList.add(order);
        orderList.remove(same);

        assertEquals(orderList.findById(order.getId()), null);
    }

    @Test
    public void findById() {
        OrderEntry order = OrderEntry.builder()
                .id(random.nextInt()).price(random.nextInt()).timestamp(random.nextLong()).build();

        orderList.add(order);

        assertEquals(orderList.findById(order.getId()), order);
    }

    @Test
    public void findByMaxPrice() {
        OrderEntry order = OrderEntry.builder()
                .id(random.nextInt()).price(9001).timestamp(random.nextLong()).build();

        orderList.add(order);

        assertEquals(orderList.findByMaxPrice(), order);
    }
    @Test
    public void findByMinPrice() {
        OrderEntry order = OrderEntry.builder()
                .id(random.nextInt()).price(1).timestamp(random.nextLong()).build();

        orderList.add(order);

        assertEquals(orderList.findByMinPrice(), order);
    }

    @Test
    public void findByMaxPriceReturnsEarliestTimestamp() {
        OrderEntry order = OrderEntry.builder()
                .id(1001).price(9000).timestamp(100L).client("c1").broker("b1").build();

        OrderEntry late = order.toBuilder().id(1000).timestamp(1473L).client("c1").broker("b1").build();

        orderList.add(late);
        orderList.add(order);

        assertEquals(orderList.findByMaxPrice(), order);
    }

    @Test
    public void findByMinPriceReturnsEarliestTimestamp() {
        OrderEntry order = OrderEntry.builder()
                .id(1000).price(100).timestamp(100L).client("c1").broker("b1").build();

        OrderEntry late = order.toBuilder().id(1001).timestamp(1473L).client("c1").broker("b1").build();

        orderList.add(late);
        orderList.add(order);

        assertEquals(orderList.findByMinPrice(), order);
    }

    @Test
    public void findByPriceUpTo() {
        OrderEntryStubber stub = new OrderEntryStubber();

        orderList.add(stub.of(100, 10));
        orderList.add(stub.of(100, 10));
        orderList.add(stub.of(100, 10));
        orderList.add(stub.of(100, 10));
        orderList.add(stub.of(100, 8));
        orderList.add(stub.of(100, 11));

        assertEquals(orderList.findByPriceUpTo(10), Arrays.asList(
                stub.of(100, 8),
                stub.of(100, 10),
                stub.of(100, 10),
                stub.of(100, 10),
                stub.of(100, 10)
        ));
    }

    @Test
    public void findByPriceDownTo() {
        OrderEntryStubber stub = new OrderEntryStubber();

        orderList.add(stub.of(100, 10010));
        orderList.add(stub.of(100, 10010));
        orderList.add(stub.of(100, 10010));
        orderList.add(stub.of(100, 10010));
        orderList.add(stub.of(100, 10008));
        orderList.add(stub.of(100, 10011));
        orderList.add(stub.of(100, 10011));

        assertEquals(orderList.findByPriceDownTo(10010), Arrays.asList(
                stub.of(100, 10011),
                stub.of(100, 10011),
                stub.of(100, 10010),
                stub.of(100, 10010),
                stub.of(100, 10010),
                stub.of(100, 10010)
        ));
    }

    @Test
    public void removeByBrokerAndId() {
        OrderEntry order = OrderEntry.builder()
                .id(random.nextInt()).price(random.nextInt()).timestamp(random.nextLong()).broker("unique").build();

        orderList.add(order);

        orderList.removeByBrokerAndId(order.getBroker(), order.getId());
        assertFalse(orderList.entries().contains(order));
    }

    @Test
    public void findByPriceDownToReturnsEmptyCollectionOnFreshList() {
        OrderList orderList = createListInstance();

        assertNotNull(orderList.findByPriceDownTo(100));
    }

    @Test
    public void findByPriceUpToReturnsEmptyCollectionOnFreshList() {
        OrderList orderList = createListInstance();

        assertNotNull(orderList.findByPriceUpTo(100));
    }

    @Test
    public void findByPriceDownToRemovesEntries() {
        OrderEntryStubber stub = new OrderEntryStubber();

        // Given
        OrderEntry entry = stub.of(100, 10010);
        orderList.add(entry);

        Set<OrderEntry> entriesBefore = new HashSet<>(orderList.entries());

        // When
        Set<OrderEntry> entriesRemoved = new HashSet<>(orderList.findByPriceDownTo(10010));

        // Then
        Set<OrderEntry> entriesAfter = new HashSet<>(orderList.entries());
        assertTrue(Sets.intersection(entriesAfter, entriesRemoved).isEmpty());
        assertEquals(Sets.union(entriesAfter, entriesRemoved), entriesBefore);
    }

    @Test
    public void findByPriceUpToRemovesEntries() {
        OrderEntryStubber stub = new OrderEntryStubber();

        // Given
        OrderEntry entry = stub.of(100, 10010);
        orderList.add(entry);

        Set<OrderEntry> entriesBefore = new HashSet<>(orderList.entries());

        // When
        Set<OrderEntry> entriesRemoved = new HashSet<>(orderList.findByPriceDownTo(10010));

        // Then
        Set<OrderEntry> entriesAfter = new HashSet<>(orderList.entries());
        assertTrue(Sets.intersection(entriesAfter, entriesRemoved).isEmpty());
        assertEquals(Sets.union(entriesAfter, entriesRemoved), entriesBefore);
    }
}