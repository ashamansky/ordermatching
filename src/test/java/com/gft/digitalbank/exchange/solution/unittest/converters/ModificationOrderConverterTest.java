package com.gft.digitalbank.exchange.solution.unittest.converters;

import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.solution.converters.BrokerMessageConverter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Kemal_Demir on 12/7/2016.
 */
public class ModificationOrderConverterTest {

    @DataProvider
    public Object[][] validModificationOrder() {
        return BrokerMessageDataProvider.getValidModificationOrder();
    }

    @Test(dataProvider = "validModificationOrder")
    public void shouldModificationOrderHaveCorrectStructure(String jsonFormat) {
        // Given
        BrokerMessageConverter brokerMessageConverter = new BrokerMessageConverter();

        // When
        ModificationOrder actual = (ModificationOrder) brokerMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual.getMessageType());
        assertNotNull(actual.getId());
        assertNotNull(actual.getTimestamp());
        assertNotNull(actual.getBroker());
        assertNotNull(actual.getModifiedOrderId());
        assertNotNull(actual.getDetails());
        assertNotNull(actual.getDetails().getAmount());
        assertNotNull(actual.getDetails().getPrice());
    }
}
