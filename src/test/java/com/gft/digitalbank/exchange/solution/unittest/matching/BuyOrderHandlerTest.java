package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.solution.matching.OrderBookService;
import com.gft.digitalbank.exchange.solution.matching.OrderList;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.matching.TransactionService;
import com.gft.digitalbank.exchange.solution.matching.handlers.BuyOrderHandler;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMapper;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderEntryStubber;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderRegistryStubs;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;


public class BuyOrderHandlerTest{

    @Mock
    private OrderBookService orderBookService;

    @Mock
    private TransactionService transactionService;

    @Mock
    private OrderMapper orderMapper;

    @InjectMocks
    private BuyOrderHandler buyOrderHandler;

    private OrderEntryStubber stub;

    @BeforeMethod
    public void setUp() {
        this.stub = new OrderEntryStubber();
        this.buyOrderHandler = new BuyOrderHandler();
        initMocks(this);
    }

    @Test(dataProvider = "orderRegistryStubs", dataProviderClass = OrderRegistryStubs.class)
    public void noMatchGivesNoTransaction(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        OrderRegistry orderRegistry = registry.get("ABC");
        buyOrderHandler.handle(stub.of(1,1), orderRegistry.getBuyEntries(), orderRegistry.getSellEntries());

        verify(transactionService, never()).log(anyString(), any(OrderEntry.class), any(OrderEntry.class));
    }

    @Test(dataProvider = "orderRegistryStubs", dataProviderClass = OrderRegistryStubs.class)
    public void noMatchAddsOrder(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        OrderRegistry registryStub = registry.get("ABC");
        OrderList buyEntries = mock(OrderList.class);

        buyOrderHandler.handle(stub.of(1, 1), buyEntries, registryStub.getSellEntries());

        verify(buyEntries, times(1)).add(any(OrderEntry.class));
    }

    @DataProvider
    public static Object[][] fullyMatchingOrderPairs() {
        final OrderEntryStubber stub = new OrderEntryStubber();
        final List<OrderEntry> details = Arrays.asList(
                stub.of(5, 300),
                stub.of(50, 400),
                stub.of(1, 1)
        );

        List<Object[]> cases =  details.stream()
                .map(entry -> new Object[]{entry, entry})
                .collect(Collectors.toList());

        return cases.toArray(new Object[cases.size()][]);
    }

    @Test(dataProvider = "fullyMatchingOrderPairs")
    public void fullMatchAddsTransaction(OrderEntry buyOrder, OrderEntry matchingEntry) {
        OrderList sellEntries = mock(OrderList.class);

        when(sellEntries.findByPriceUpTo(buyOrder.getPrice()))
                .thenReturn(Collections.singletonList(matchingEntry));

        assertEquals(buyOrderHandler.handle(buyOrder, mock(OrderList.class), sellEntries), Collections.singletonList(matchingEntry));
    }

    @DataProvider
    public static Object[][] sellOrdersFulfillingWholeBuyOrders() {
        final OrderEntryStubber stub = new OrderEntryStubber();

        return new OrderEntry[][]{
                {
                        stub.of(100, 100),  // Buy order
                        stub.of(300, 100),  // Sell order
                        stub.of(200, 100),  // Expected remaining sell order
                        stub.of(100, 100)   // Expected match
                },
                {
                        stub.of(1, 10),   // Buy order
                        stub.of(30, 10),  // Sell order
                        stub.of(29, 10),  // Expected sell order
                        stub.of(1, 10)    // Expected match
                },
                {
                        stub.of(100, 120),  // Buy order
                        stub.of(3000, 120), // Sell order
                        stub.of(2900, 120), // Expected sell order
                        stub.of(100, 120)  // Expected match
                },
                {
                        stub.of(100, 120),  // Buy order
                        stub.of(3000, 110), // Sell order
                        stub.of(2900, 110), // Expected sell order
                        stub.of(100, 110)  // Expected match
                }
        };
    }

    @Test(dataProvider = "sellOrdersFulfillingWholeBuyOrders")
    public void fulfilledBuyOrderUpdatesSellOrder(OrderEntry buyOrder, OrderEntry matchingEntry, OrderEntry expectedEntry, OrderEntry match) {
        OrderList sellEntries = mock(OrderList.class);
        ArgumentCaptor<OrderEntry> sellOrderNew = ArgumentCaptor.forClass(OrderEntry.class);

        when(sellEntries.findByPriceUpTo(buyOrder.getPrice()))
                .thenReturn(Collections.singletonList(matchingEntry), null);

        buyOrderHandler.handle(buyOrder, mock(OrderList.class), sellEntries);

        verify(sellEntries, times(1)).add(sellOrderNew.capture());
        assertEquals(sellOrderNew.getValue().getAmount(), expectedEntry.getAmount());
        assertEquals(sellOrderNew.getValue().getPrice(), expectedEntry.getPrice());
    }

    @Test(dataProvider = "sellOrdersFulfillingWholeBuyOrders")
    public void fulfilledBuyOrderReturnsMatchedPriceAndAmount(OrderEntry buyOrder, OrderEntry matchingEntry, OrderEntry expectedEntry, OrderEntry match) {
        OrderList sellEntries = mock(OrderList.class);

        when(sellEntries.findByPriceUpTo(buyOrder.getPrice()))
                .thenReturn(Collections.singletonList(matchingEntry), null);

        assertEquals(buyOrderHandler.handle(buyOrder, mock(OrderList.class), sellEntries), Collections.singletonList(match));
    }

    @DataProvider
    public static Object[][] multipleSellOrdersFulfillingWholeBuyOrders() {
        final OrderEntryStubber stub = new OrderEntryStubber();
        return new OrderEntry[][][]{
                {
                        { stub.of(100, 100) },  // Buy order
                        { stub.of(50, 100), stub.of(50, 100) },  // Sell orders
                        { stub.of(50, 100), stub.of(50, 100) },   // Expected removed orders
                        {} // Expected added sell orders
                },
                {
                        { stub.of(100, 10) },    // Buy order
                        { stub.of(30, 10), stub.of(20, 10), stub.of(50, 10) },  // Sell orders
                        { stub.of(30, 10), stub.of(20, 10), stub.of(50, 10) },  // Expected sell orders removed
                        {} // Expected added sell orders
                },
                {
                        { stub.of(100, 120)  },  // Buy order
                        { stub.of(50, 10), stub.of(50, 110) }, // Sell orders
                        { stub.of(50, 10), stub.of(50, 110) }, // Expected sell orders removed
                        { } // Expected added sell orders
                },
                {
                        { stub.of(10, 1200)  },  // Buy order
                        { stub.of(5, 1100), stub.of(15, 1200) }, // Sell orders
                        { stub.of(5, 1100), stub.of(5, 1200) }, // Expected sell orders removed
                        { stub.of(10, 1200) } // Expected added sell orders
                },
                {
                        { stub.of(50, 200)  },  // Buy order
                        { stub.of(15, 100), stub.of(40, 200), stub.of(10, 200) }, // Sell orders
                        { stub.of(15, 100), stub.of(35, 200)  }, // Expected sell orders removed
                        { stub.of(5, 200), stub.of(10, 200) } // Expected added sell orders
                }
        };
    }

    @Test(dataProvider = "multipleSellOrdersFulfillingWholeBuyOrders")
    public void lastPartialMatchAreUpdated(OrderEntry[] buyOrder, OrderEntry[] sellOrders, OrderEntry[] expectedRemovals, OrderEntry[] expectedAdditions) {
        OrderList sellEntries = mock(OrderList.class);
        ArgumentCaptor<OrderEntry> added = ArgumentCaptor.forClass(OrderEntry.class);

        when(sellEntries.findByPriceUpTo(buyOrder[0].getPrice()))
                .thenReturn(Arrays.asList(sellOrders), null);

        buyOrderHandler.handle(buyOrder[0], mock(OrderList.class), sellEntries);

        verify(sellEntries, times(expectedAdditions.length)).add(added.capture());
        assertEquals(added.getAllValues(), Arrays.asList(expectedAdditions));
    }

    @Test(dataProvider = "multipleSellOrdersFulfillingWholeBuyOrders")
    public void buyOrderAddsTransactionPerMatch(OrderEntry[] buyOrder, OrderEntry[] sellOrders, OrderEntry[] expectedRemovals, OrderEntry[] expectedAdditions) {
        OrderList sellEntries = mock(OrderList.class);

        when(sellEntries.findByPriceUpTo(buyOrder[0].getPrice()))
                .thenReturn(Arrays.asList(sellOrders), null);

        assertEquals(buyOrderHandler.handle(buyOrder[0], mock(OrderList.class), sellEntries), Arrays.asList(expectedRemovals));
    }

    @DataProvider
    public static Object[][] multipleSellOrdersNotMatchedDueToOversupply() {
        final OrderEntryStubber stub = new OrderEntryStubber();
        return new OrderEntry[][][]{
                {
                        { stub.of(100, 100) },  // Buy order
                        { stub.of(50, 100), stub.of(50, 100), stub.of(50, 100) },  // Sell orders
                        { stub.of(50, 100) } // Expected sell orders left
                },
                {
                        { stub.of(100, 10) },    // Buy order
                        { stub.of(10, 8), stub.of(30, 10), stub.of(20, 10), stub.of(50, 10), stub.of(50, 10) },  // Sell orders
                        { stub.of(10, 10), stub.of(50, 10) }
                },
                {
                        { stub.of(100, 120)  },  // Buy order
                        { stub.of(30, 1), stub.of(50, 10), stub.of(50, 110) }, // Sell orders
                        { stub.of(30, 110) },
                },
                {
                        { stub.of(10, 1200)  },  // Buy order
                        { stub.of(100, 100), stub.of(5, 1100), stub.of(15, 1200) }, // Sell orders
                        { stub.of(90, 100), stub.of(5, 1100), stub.of(15, 1200) }, // Sell orders
                },
                {
                        { stub.of(50, 200)  },  // Buy order
                        { stub.of(15, 100), stub.of(40, 200), stub.of(10, 200) }, // Sell orders
                        { stub.of(5, 200), stub.of(10, 200) }, // Sell orders
                }
        };
    }
    @Test(dataProvider = "multipleSellOrdersNotMatchedDueToOversupply")
    public void buyOrderLeavesRemainder(OrderEntry[] buyOrder, OrderEntry[] sellOrders, OrderEntry[] expectedRemainder) {
        // Given
        OrderList sellEntries = mock(OrderList.class);
        ArgumentCaptor<OrderEntry> addedEntries = ArgumentCaptor.forClass(OrderEntry.class);

        // When
        when(sellEntries.findByPriceUpTo(buyOrder[0].getPrice()))
                .thenReturn(Arrays.asList(sellOrders), null);

        buyOrderHandler.handle(buyOrder[0], mock(OrderList.class), sellEntries);

        // Then
        verify(sellEntries, times(expectedRemainder.length)).add(addedEntries.capture());

        assertEquals(addedEntries.getAllValues(), Arrays.asList(expectedRemainder));
    }
}