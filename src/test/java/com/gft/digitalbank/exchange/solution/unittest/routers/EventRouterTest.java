package com.gft.digitalbank.exchange.solution.unittest.routers;

import com.gft.digitalbank.exchange.solution.events.*;
import com.gft.digitalbank.exchange.solution.processors.impl.MatchingProcessor;
import com.gft.digitalbank.exchange.solution.routers.EventRouter;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;

public class EventRouterTest {

    @Mock
    MatchingProcessor matchingProcessor;

    @InjectMocks
    EventRouter eventRouter = new EventRouter();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        eventRouter.unregisterAllProcessors();
        eventRouter.register(matchingProcessor);
    }


    @Test
    public void testUnregisterProcessor() {
        Assert.assertFalse(eventRouter.getProcessors().isEmpty());
        eventRouter.unregisterProcessor(matchingProcessor);
        Assert.assertTrue(eventRouter.getProcessors().isEmpty());
    }

    @Test
    public void testUnregisterAllProcessors() {
        Assert.assertFalse(eventRouter.getProcessors().isEmpty());
        eventRouter.unregisterAllProcessors();
        Assert.assertTrue(eventRouter.getProcessors().isEmpty());

    }

    @Test
    public void testRegisterProcessor() {
        Assert.assertTrue(eventRouter.getProcessors().contains(matchingProcessor));
        Assert.assertEquals(1, eventRouter.getProcessors().size());
    }

    @Test
    public void testBuyRoute() {
        BuyEvent event = new BuyEvent(null,1);
        eventRouter.route(event);
        verify(matchingProcessor).handle(event);
    }

    @Test
    public void testSellRoute() {
        SellEvent event = new SellEvent(null,1);
        eventRouter.route(event);
        verify(matchingProcessor).handle(event);
    }

    @Test
    public void testModificationRoute() {
        ModificationEvent event = new ModificationEvent(null,1);
        eventRouter.route(event);
        verify(matchingProcessor).handle(event);
    }

    @Test
    public void testCancelRoute() {
        CancellationEvent event = new CancellationEvent(null,1);
        eventRouter.route(event);
        verify(matchingProcessor).handle(event);
    }

    @Test
    public void testShutdownRoute() {
        ShutdownEvent event = new ShutdownEvent(null,1);
        eventRouter.route(event);
        verify(matchingProcessor).handle(event);
    }


}
