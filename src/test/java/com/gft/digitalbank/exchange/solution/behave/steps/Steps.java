package com.gft.digitalbank.exchange.solution.behave.steps;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Created by Kemal_Demir on 14/7/2016.
 */
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
@Component
@interface Steps {

}
