package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.solution.events.CancellationEvent;
import com.gft.digitalbank.exchange.solution.matching.handlers.CancellationOrderHandler;
import com.gft.digitalbank.exchange.solution.matching.OrderBookService;
import com.gft.digitalbank.exchange.solution.matching.OrderHandler;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.EventStubber;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderRegistryStubs;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.*;


public class CancellationOrderHandlerTest {

    private EventStubber stub;

    @Mock
    private OrderBookService orderBookService;

    @InjectMocks
    private OrderHandler<CancellationEvent> cancellationOrderHandler = new CancellationOrderHandler();

    @BeforeMethod
    public void setUp() {
        this.stub = new EventStubber();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldNotCancelOrderWhenNoOrderBookFound() {
        this.cancellationOrderHandler.handle(this.stub.ofCancellationEvent(1));

        // Then
        verify(this.orderBookService).cancelByBrokerAndId("broker1", 1);
    }

    @Test(dataProvider = "emptyOrderRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldNotCancelOrderWhenOrderBookEmpty(Map<String, OrderRegistry> registry, Set<OrderBook> books) {

        // When
        this.cancellationOrderHandler.handle(this.stub.ofCancellationEvent(1));

        // Then
        verify(this.orderBookService).cancelByBrokerAndId("broker1", 1);
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldNotCancelOrderWhenNoOrderFound(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get("ABC"));

        // When
        this.cancellationOrderHandler.handle(this.stub.ofCancellationEvent(400));

        // Then
        Assert.assertEquals(3, registry.get("ABC").getBuyEntries().entries().size());
        Assert.assertEquals(3, registry.get("ABC").getSellEntries().entries().size());
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldCancelBuyOrderWhenOrderFound(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get("ABC"));

        // When
        CancellationEvent event = this.stub.ofCancellationEvent(1);
        this.cancellationOrderHandler.handle(event);
        // Then
        verify(orderBookService).cancelByBrokerAndId(event.getOrder().getBroker(), event.getOrder().getCancelledOrderId());
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldCancelSellOrderWhenOrderFound(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get("ABC"));

        // When
        CancellationEvent event = this.stub.ofCancellationEvent(4);
        this.cancellationOrderHandler.handle(event);

        // Then
        verify(orderBookService).cancelByBrokerAndId(event.getOrder().getBroker(), event.getOrder().getCancelledOrderId());
    }
}
