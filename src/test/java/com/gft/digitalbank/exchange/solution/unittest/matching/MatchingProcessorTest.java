package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.solution.events.*;
import com.gft.digitalbank.exchange.solution.matching.handlers.*;
import com.gft.digitalbank.exchange.solution.processors.EventProcessor;
import com.gft.digitalbank.exchange.solution.processors.impl.MatchingProcessor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class MatchingProcessorTest {

    @Mock
    BuyOrderHandler buyOrderHandler;

    @Mock
    SellOrderHandler sellOrderHandler;

    @Mock
    CancellationOrderHandler cancellationOrderHandler;

    @Mock
    ModificationOrderHandler modificationOrderHandler;

    @Mock
    ShutdownOrderHandler shutdownOrderHandler;


    @InjectMocks
    EventProcessor matchingProcessor = new MatchingProcessor();


    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testBuyHandlerExecuted() {
        matchingProcessor.handle(new BuyEvent(null,1));
        verify(buyOrderHandler).handle(any(BuyEvent.class));
    }

    @Test
    public void testSellHandlerExecuted() {
        matchingProcessor.handle(new SellEvent(null,1));
        verify(sellOrderHandler).handle(any(SellEvent.class));
    }

    @Test
    public void testModificationHandlerExecuted() {
        matchingProcessor.handle(new ModificationEvent(null,1));
        verify(modificationOrderHandler).handle(any(ModificationEvent.class));
    }

    @Test
    public void testCancellationHandlerExecuted() {
        matchingProcessor.handle(new CancellationEvent(null,1));
        verify(cancellationOrderHandler).handle(any(CancellationEvent.class));
    }

    @Test
    public void testShutdownHandlerExecuted() {
        matchingProcessor.handle(new ShutdownEvent(null,1));
        verify(shutdownOrderHandler).handle(any(ShutdownEvent.class));

    }
}
