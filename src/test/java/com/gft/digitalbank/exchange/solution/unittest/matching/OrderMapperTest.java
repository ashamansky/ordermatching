package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.OrderDetails;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.matching.sequence.OrderIDCreator;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMapper;
import lombok.val;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;


public class OrderMapperTest {

    @Mock
    private OrderIDCreator orderIdCreator;

    @InjectMocks
    private OrderMapper orderMapper;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @DataProvider
    public static Object[][] stubTestCases() {
        return new Object[][]{
                {OrderEntry.builder().id(1).price(1).client("client").amount(1).timestamp(10L).broker("broker1").build()},
                {OrderEntry.builder().id(2).price(2).client("client1").amount(3).timestamp(12L).broker("broker2").build()}
        };
    }

    @Test(dataProvider = "stubTestCases")
    public void convertToGftCarriesOverAllFields(OrderEntry stub) {
        val entry = orderMapper.convert(stub);
        assertEquals(entry.getId(), stub.getId());
        assertEquals(entry.getPrice(), stub.getPrice());
        assertEquals(entry.getAmount(), stub.getAmount());
        assertEquals(entry.getBroker(), stub.getBroker());
        assertEquals(entry.getClient(), stub.getClient());
    }

    @DataProvider
    public static Object[][] positionOrderTestCases() {
        return new Object[][]{
                {PositionOrder.builder().id(1).details(OrderDetails.builder().price(1).amount(1).build()).side(Side.BUY).client("client").timestamp(10L).broker("broker1").product("ABC").build()},
                {PositionOrder.builder().id(1).details(OrderDetails.builder().price(2).amount(3).build()).side(Side.SELL).client("client1").timestamp(12L).broker("broker2").product("DEF").build()}
        };
    }

    @Test(dataProvider = "positionOrderTestCases")
    public void convertFromMessageCarriesOverAllFields(PositionOrder stub) {
        when(this.orderIdCreator.getNewIdFor(stub)).thenCallRealMethod();
        OrderEntry entry = orderMapper.convert(stub);
        assertEquals(entry.getId(), stub.getId());
        assertEquals(entry.getPrice(), stub.getDetails().getPrice());
        assertEquals(entry.getAmount(), stub.getDetails().getAmount());
        assertEquals(entry.getBroker(), stub.getBroker());
        assertEquals(entry.getClient(), stub.getClient());
    }
}