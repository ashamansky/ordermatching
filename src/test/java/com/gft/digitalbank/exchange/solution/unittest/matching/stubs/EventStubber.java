package com.gft.digitalbank.exchange.solution.unittest.matching.stubs;

import com.gft.digitalbank.exchange.model.OrderDetails;
import com.gft.digitalbank.exchange.model.orders.MessageType;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.events.*;

public class EventStubber {
    private static final String A_BROKER = "broker1";
    private static final String A_CLIENT = "client1";
    private static final String A_PRODUCT = "ABC";

    private int id;
    private long timestamp;

    public EventStubber() {
        reset();
    }

    public EventStubber reset() {
        id = 1;
        timestamp = 1L;
        return this;
    }

    public BuyEvent ofBuyEvent(int amount, int price) {
        return new BuyEvent(createPositionOrder(amount, price)
                .side(Side.BUY)
                .product(A_PRODUCT)
                .build()
        ,1);
    }

    public BuyEvent ofBuyEvent(int amount, int price, String product) {
        return new BuyEvent(createPositionOrder(amount, price)
                .side(Side.BUY)
                .product(product)
                .build()
        , 1);
    }

    public SellEvent ofSellEvent(int amount, int price) {
        return new SellEvent(createPositionOrder(amount, price)
                .side(Side.SELL)
                .product(A_PRODUCT)
                .build(), 1);
    }

    public SellEvent ofSellEvent(int amount, int price, String product) {
        return new SellEvent(createPositionOrder(amount, price)
                .side(Side.SELL)
                .product(product)
                .build()
        ,1);
    }

    protected PositionOrder.PositionOrderBuilder createPositionOrder(int amount, int price) {
        return PositionOrder.builder()
                .id(id)
                .details(OrderDetails.builder()
                        .price(price)
                        .amount(amount)
                        .build()
                )
                .broker(A_BROKER)
                .client(A_CLIENT)
                .timestamp(timestamp);
    }

    public CancellationEvent ofCancellationEvent(int cancelledOrderId) {
        return new CancellationEvent(com.gft.digitalbank.exchange.model.orders.CancellationOrder.builder()
                .id(id)
                .timestamp(timestamp)
                .messageType(MessageType.CANCEL)
                .cancelledOrderId(cancelledOrderId)
                .broker(A_BROKER)
                .build()
        ,1);
    }

    public ModificationEvent ofModificationEvent(int id, int modifiedOrderId, int amount, int price, String broker) {
        return new ModificationEvent(com.gft.digitalbank.exchange.model.orders.ModificationOrder.builder()
                .id(id)
                .timestamp(timestamp)
                .details(OrderDetails.builder()
                        .price(price)
                        .amount(amount)
                        .build()
                )
                .modifiedOrderId(modifiedOrderId)
                .broker(broker)
                .build()
       ,1 );
    }

    public ShutdownEvent ofShutdownEvent() {
        return new ShutdownEvent(null, 1);
    }

    public EventStubber inc() {
        ++id;
        ++timestamp;
        return this;
    }
}
