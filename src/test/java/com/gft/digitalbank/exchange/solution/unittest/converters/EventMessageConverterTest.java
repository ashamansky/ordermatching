package com.gft.digitalbank.exchange.solution.unittest.converters;

import com.gft.digitalbank.exchange.model.orders.MessageType;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.converters.BrokerMessageConverter;
import com.gft.digitalbank.exchange.solution.converters.EventMessageConverter;
import com.gft.digitalbank.exchange.solution.events.*;
import com.gft.digitalbank.exchange.solution.exceptions.InvalidBrokerMessageException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertNull;

public class EventMessageConverterTest {

    @Mock
    private BrokerMessageConverter brokerMessageConverter;

    @InjectMocks
    private EventMessageConverter eventMessageConverter = new EventMessageConverter();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = InvalidBrokerMessageException.class,
            dataProvider = "invalidBuyOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldThrowExceptionWhenInvalidBuyOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        BuyEvent actual = (BuyEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotEquals(actual.getPositionOrder().getSide(), Side.BUY);
        assertNotEquals(actual.getPositionOrder().getMessageType(),MessageType.ORDER);
    }

    @Test(dataProvider = "validBuyOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldConvertWhenValidBuyOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        BuyEvent actual = (BuyEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual);
        assertEquals(actual.getPositionOrder().getSide(), Side.BUY);
        assertEquals(actual.getPositionOrder().getMessageType(), MessageType.ORDER);
    }

    @Test(expectedExceptions = InvalidBrokerMessageException.class,
            dataProvider = "invalidSellOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldThrowExceptionWhenInvalidSellOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        SellEvent actual = (SellEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotEquals(actual.getPositionOrder().getSide(), Side.SELL);
        assertNotEquals(actual.getPositionOrder().getMessageType(), MessageType.ORDER);
    }

    @Test(dataProvider = "validSellOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldConvertWhenValidSellOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        SellEvent actual = (SellEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual);
        assertEquals(actual.getPositionOrder().getSide(), Side.SELL);
        assertEquals(actual.getPositionOrder().getMessageType(), MessageType.ORDER);
    }

    @Test(expectedExceptions = InvalidBrokerMessageException.class,
            dataProvider = "invalidModificationOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldThrowExceptionWhenInvalidModificationOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        ModificationEvent actual = (ModificationEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNull(actual.getOrder().getModifiedOrderId());
        assertNotEquals(actual.getOrder().getMessageType(), MessageType.MODIFICATION);
    }

    @Test(dataProvider = "validModificationOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldConvertWhenValidModificationOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        ModificationEvent actual = (ModificationEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual);
        assertNotNull(actual.getOrder().getModifiedOrderId());
        assertEquals(actual.getOrder().getMessageType(), MessageType.MODIFICATION);
    }

    @Test(expectedExceptions = InvalidBrokerMessageException.class,
            dataProvider = "invalidCancellationOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldThrowExceptionWhenInvalidCancellationOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        CancellationEvent actual = (CancellationEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNull(actual.getOrder().getCancelledOrderId());
        assertNotEquals(actual.getOrder().getMessageType(), MessageType.CANCEL);
    }

    @Test(dataProvider = "validCancellationOrder", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldConvertWhenValidCancellationOrder(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        CancellationEvent actual = (CancellationEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual);
        assertNotNull(actual.getOrder().getCancelledOrderId());
        assertEquals(actual.getOrder().getMessageType(), MessageType.CANCEL);
    }

    @Test(expectedExceptions = InvalidBrokerMessageException.class,
            dataProvider = "invalidShutdownNotification", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldThrowExceptionWhenInvalidShutdownNotification(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        ShutdownEvent actual = (ShutdownEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotEquals(actual.getNotification().getMessageType(), MessageType.SHUTDOWN_NOTIFICATION);
    }

    @Test(dataProvider = "validShutdownNotification", dataProviderClass = BrokerMessageDataProvider.class)
    public void shouldConvertWhenValidShutdownNotification(String jsonFormat) {
        // Given
        when(this.brokerMessageConverter.convert(anyString())).thenCallRealMethod();

        // When
        ShutdownEvent actual = (ShutdownEvent) eventMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual);
        assertEquals(actual.getNotification().getMessageType(),MessageType.SHUTDOWN_NOTIFICATION);
    }
}