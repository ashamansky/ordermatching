package com.gft.digitalbank.exchange.solution.behave.stories;

import com.gft.digitalbank.exchange.solution.behave.AcceptanceTestConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Kemal_Demir on 18/7/2016.
 */
@ContextConfiguration(classes = AcceptanceTestConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class CancellationOfFullyMatchedBuyOrder extends AbstractSpringJBehaveStory {

}
