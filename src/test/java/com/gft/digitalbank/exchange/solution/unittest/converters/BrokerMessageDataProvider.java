package com.gft.digitalbank.exchange.solution.unittest.converters;

import org.testng.annotations.DataProvider;

import java.util.stream.Stream;


public class BrokerMessageDataProvider {

    @DataProvider(name = "validBrokerMessages")
    public static Object[][] getValidBrokerMessages() {
        return Stream.of(
                getValidPositionOrder(),
                getValidCancellationOrder(),
                getValidModificationOrder(),
                getValidShutdownNotification()
        ).flatMap(Stream::of).toArray(Object[][]::new);
    }

    @DataProvider(name = "inValidBrokerMessages")
    public static Object[][] getInvalidBrokerMessages() {
        return Stream.of(
                getInvalidPositionOrder(),
                getInvalidCancellationOrder(),
                getInvalidModificationOrder(),
                getInvalidShutdownNotification()
        ).flatMap(Stream::of).toArray(Object[][]::new);
    }

    @DataProvider(name = "invalidPositionOrder")
    public static Object[][] getInvalidPositionOrder() {
        return Stream.of(
                getInvalidBuyOrder(),
                getInvalidSellOrder()
        ).flatMap(Stream::of).toArray(Object[][]::new);
    }

    @DataProvider(name = "invalidBuyOrder")
    public static Object[][] getInvalidBuyOrder() {
        return new Object[][]{
                {""},
                {"a"},
                {"123"},
                {"invalid position order"}
        };
    }

    @DataProvider(name = "invalidSellOrder")
    public static Object[][] getInvalidSellOrder() {
        return new Object[][]{
                {""},
                {"a"},
                {"123"},
                {"invalid position order"}
        };
    }

    @DataProvider(name = "validPositionOrder")
    public static Object[][] getValidPositionOrder() {
        return Stream.of(
                getValidBuyOrder(),
                getValidSellOrder()
        ).flatMap(Stream::of).toArray(Object[][]::new);
    }

    @DataProvider(name = "validBuyOrder")
    public static Object[][] getValidBuyOrder() {
        return new Object[][]{
                {"{\"messageType\": \"ORDER\",\"side\": \"BUY\",\"id\": 1,\"timestamp\": 1,\"broker\": \"b001\",\"client\": \"c001\",\"product\": \"abc\",\"details\": {\"amount\": 10,\"price\": 90}}"},
                {"{\"messageType\": \"ORDER\",\"side\": \"BUY\",\"id\": 2,\"timestamp\": 2,\"broker\": \"b002\",\"client\": \"c002\",\"product\": \"abc2\",\"details\": {\"amount\": 12,\"price\": 92}}"}
        };
    }

    @DataProvider(name = "validSellOrder")
    public static Object[][] getValidSellOrder() {
        return new Object[][]{
                {"{\"messageType\": \"ORDER\",\"side\": \"SELL\",\"id\": 1,\"timestamp\": 1,\"broker\": \"b001\",\"client\": \"c001\",\"product\": \"abc\",\"details\": {\"amount\": 10,\"price\": 90}}"},
                {"{\"messageType\": \"ORDER\",\"side\": \"SELL\",\"id\": 2,\"timestamp\": 2,\"broker\": \"b002\",\"client\": \"c002\",\"product\": \"abc2\",\"details\": {\"amount\": 12,\"price\": 92}}"}
        };
    }

    @DataProvider(name = "invalidModificationOrder")
    public static Object[][] getInvalidModificationOrder() {
        return new Object[][]{
                {""},
                {"a"},
                {"123"},
                {"invalid modification order"}
        };
    }

    @DataProvider(name = "validModificationOrder")
    public static Object[][] getValidModificationOrder() {
        return new Object[][]{
                {"{\"messageType\": \"MODIFICATION\",\"id\": 3,\"timestamp\": 3,\"broker\": \"b001\",\"modifiedOrderId\": 1,\"details\": {\"amount\": 10,\"price\": 90}}"},
                {"{\"messageType\": \"MODIFICATION\",\"id\": 4,\"timestamp\": 4,\"broker\": \"b002\",\"modifiedOrderId\": 2,\"details\": {\"amount\": 12,\"price\": 92}}"}
        };
    }

    @DataProvider(name = "invalidCancellationOrder")
    public static Object[][] getInvalidCancellationOrder() {
        return new Object[][]{
                {""},
                {"a"},
                {"123"},
                {"invalid cancellation order"}
        };
    }

    @DataProvider(name = "validCancellationOrder")
    public static Object[][] getValidCancellationOrder() {
        return new Object[][]{
                {"{\"messageType\": \"CANCEL\",\"id\": 4,\"timestamp\": 4,\"broker\": \"b001\",\"cancelledOrderId\": 2}"},
                {"{\"messageType\": \"CANCEL\",\"id\": 5,\"timestamp\": 5,\"broker\": \"b002\",\"cancelledOrderId\": 3}"}
        };
    }

    @DataProvider(name = "invalidShutdownNotification")
    public static Object[][] getInvalidShutdownNotification() {
        return new Object[][]{
                {""},
                {"a"},
                {"123"},
                {"invalid shutdown notification"}
        };
    }

    @DataProvider(name = "validShutdownNotification")
    public static Object[][] getValidShutdownNotification() {
        return new Object[][]{
                {"{\"messageType\": \"SHUTDOWN_NOTIFICATION\"}"}
        };
    }
}
