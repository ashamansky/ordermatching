package com.gft.digitalbank.exchange.solution.unittest.converters;

import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.solution.converters.BrokerMessageConverter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Kemal_Demir on 12/7/2016.
 */
public class PositionOrderConverterTest {

    @DataProvider
    public Object[][] validPositionOrder() {
        return BrokerMessageDataProvider.getValidPositionOrder();
    }

    @Test(dataProvider = "validPositionOrder")
    public void shouldPositionOrderHaveCorrectStructure(String jsonFormat) {
        // Given
        BrokerMessageConverter brokerMessageConverter = new BrokerMessageConverter();

        // When
        PositionOrder actual = (PositionOrder) brokerMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual.getMessageType());
        assertNotNull(actual.getSide());
        assertNotNull(actual.getId());
        assertNotNull(actual.getTimestamp());
        assertNotNull(actual.getBroker());
        assertNotNull(actual.getClient());
        assertNotNull(actual.getProduct());
        assertNotNull(actual.getDetails());
        assertNotNull(actual.getDetails().getAmount());
        assertNotNull(actual.getDetails().getPrice());
    }
}
