package com.gft.digitalbank.exchange.solution.behave.steps;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.behave.stories.params.OrderParams;
import com.gft.digitalbank.exchange.solution.behave.stories.params.TransactionParams;
import org.jbehave.core.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;


@Steps
public class SharedSteps {
    private final Logger logger = LoggerFactory.getLogger(SharedSteps.class);

    @Autowired
    private StepsAgent stepsAgent;

    @AfterScenario
    public void tearDown() {
        this.stepsAgent.clearAll();
    }

    @Given("buy entries in the order book for product $product contains: $orderParamsList")
    public void givenBuyEntries(String product, List<OrderParams> orderParamsList) {
        orderParamsList.forEach(orderEntryParams ->
                this.stepsAgent.injectBuyOrder(product, orderEntryParams));
    }

    @Given("sell entries in the order book for product $product contains: $orderParamsList")
    public void givenSellEntries(String product, List<OrderParams> orderParamsList) {
        orderParamsList.forEach(orderEntryParams ->
                this.stepsAgent.injectSellOrder(product, orderEntryParams)
        );
    }

    @When("a broker add new order with parameters: $orderParams")
    public void whenABrokerAddsNewOrder(List<OrderParams> orderParamsList) {
        orderParamsList.forEach(orderEntryParams ->
                this.stepsAgent.addNewOrder(orderEntryParams)
        );
    }

    @When("brokers add new orders with parameters: $orderParamsList")
    public void whenBrokersAddNewOrders(List<OrderParams> orderParamsList) {
        orderParamsList.forEach(this.stepsAgent::addNewOrder);
    }

    @Then("buy entries in the order book for product $product contains: $orderParamsList")
    public void thenBuyEntriesShouldContainOrders(String product, List<OrderParams> orderParamsList) {
        orderParamsList.forEach(orderEntryParams -> this.stepsAgent.checkIfOrderMatches(
                this.stepsAgent.getBuyEntry(product, orderEntryParams.getOrderId()), orderEntryParams)
        );
    }

    @Then("sell entries in the order book for product $product contains: $orderParamsList")
    public void thenSellEntriesShouldContainOrders(String product, List<OrderParams> orderParamsList) {
        orderParamsList.forEach(orderEntryParams -> this.stepsAgent.checkIfOrderMatches(
                this.stepsAgent.getSellEntry(product, orderEntryParams.getOrderId()), orderEntryParams
        ));
    }

    @Then("sell entries in the order book for product $product is empty")
    public void thenSellEntriesShouldBeEmpty(String product) {
        this.stepsAgent.checkIfOrderListIsEmpty(this.stepsAgent.getSellEntries(product));
    }

    @Then("buy entries in the order book for product $product is empty")
    public void thenBuyEntriesShouldBeEmpty(String product) {
        this.stepsAgent.checkIfOrderListIsEmpty(this.stepsAgent.getBuyEntries(product));
    }

    @Then("transaction list is empty")
    public void thenTransactionListShouldBeEmpty() {
        this.stepsAgent.checkIfTransactionListIsEmpty();
    }

    @Then("transaction list contains: $transactionParamsList")
    public void thenTransactionListShouldContainTransactions(List<TransactionParams> transactionParamsList) {
        Set<Transaction> transactions = this.stepsAgent.getTransaction();
        transactionParamsList.forEach(transactionParams -> this.stepsAgent.checkIfTransactionMatches(
                this.stepsAgent.getTransactionByBrokerAndId(transactions, transactionParams.getProduct(),
                        transactionParams.getTransactionId()), transactionParams
        ));
    }
}
