package com.gft.digitalbank.exchange.solution.unittest.converters;

import com.gft.digitalbank.exchange.model.orders.ShutdownNotification;
import com.gft.digitalbank.exchange.solution.converters.BrokerMessageConverter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Kemal_Demir on 12/7/2016.
 */
public class ShutdownNotificationConverterTest {

    @DataProvider
    public Object[][] validShutdownNotification() {
        return BrokerMessageDataProvider.getValidShutdownNotification();
    }

    @Test(dataProvider = "validShutdownNotification")
    public void shouldConvertWhenValidShutdownNotification(String jsonFormat) {
        // Given
        BrokerMessageConverter brokerMessageConverter = new BrokerMessageConverter();

        // When
        ShutdownNotification actual = (ShutdownNotification) brokerMessageConverter.convert(jsonFormat);

        // Then
        assertEquals(actual.getMessageType().toString(), "SHUTDOWN_NOTIFICATION");
    }
}
