package com.gft.digitalbank.exchange.solution.behave.stories.params;

import org.jbehave.core.annotations.AsParameters;
import org.jbehave.core.annotations.Parameter;


@AsParameters
public final class TransactionParams {

    @Parameter(name = "transaction_id")
    private Integer transactionId;

    @Parameter(name = "product")
    private String product;

    @Parameter(name = "amount")
    private Integer amount;

    @Parameter(name = "price")
    private Integer price;

    @Parameter(name = "brokerBuy")
    private String brokerBuy;

    @Parameter(name = "brokerSell")
    private String brokerSell;

    @Parameter(name = "clientBuy")
    private String clientBuy;

    @Parameter(name = "clientSell")
    private String clientSell;

    public Integer getTransactionId() {
        return transactionId;
    }

    public String getProduct() {
        return product;
    }

    public Integer getAmount() {
        return amount;
    }

    public Integer getPrice() {
        return price;
    }

    public String getBrokerBuy() {
        return brokerBuy;
    }

    public String getBrokerSell() {
        return brokerSell;
    }

    public String getClientBuy() {
        return clientBuy;
    }

    public String getClientSell() {
        return clientSell;
    }
}
