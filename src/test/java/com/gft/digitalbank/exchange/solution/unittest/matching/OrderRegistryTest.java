package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.matching.impl.OrderRegistryImpl;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.testng.Assert.assertNotNull;

public class OrderRegistryTest {
    private Class<? extends OrderRegistry> implementationClass;
    private OrderRegistry implUnderTest;

    @Factory(dataProvider="testCaseProvider")
    public OrderRegistryTest(Class<? extends OrderRegistry> implementation) {
        this.implementationClass = implementation;
    }

    @DataProvider
    public static Object[][] testCaseProvider() {
        return new Object[][]{
                {OrderRegistryImpl.class}
        };
    }

    @BeforeMethod
    public void setUp() {
        try {
            Constructor<? extends OrderRegistry> constructor = implementationClass.getConstructor(String.class);
            this.implUnderTest = constructor.newInstance("PRODUCT");
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getBuyEntriesShouldBeInitialized() {
        assertNotNull(implUnderTest.getBuyEntries());
    }

    @Test
    public void getSellEntriesShouldBeInitialized() {
        assertNotNull(implUnderTest.getSellEntries());
    }

}