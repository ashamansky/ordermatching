package com.gft.digitalbank.exchange.solution.behave.steps;

import com.gft.digitalbank.exchange.model.OrderDetails;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.model.orders.BrokerMessage;
import com.gft.digitalbank.exchange.model.orders.CancellationOrder;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.solution.behave.stories.params.OrderParams;
import com.gft.digitalbank.exchange.solution.behave.stories.params.TransactionParams;
import com.gft.digitalbank.exchange.solution.converters.EventMessageConverter;
import com.gft.digitalbank.exchange.solution.exceptions.InvalidBrokerMessageException;
import com.gft.digitalbank.exchange.solution.matching.OrderBookService;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.matching.OrderList;
import com.gft.digitalbank.exchange.solution.matching.TransactionService;
import com.gft.digitalbank.exchange.solution.routers.EventRouter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

import static com.gft.digitalbank.exchange.solution.converters.BrokerMessageConstants.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@Component
class StepsAgent {

    @Autowired
    private EventRouter eventRouter;

    @Autowired
    private OrderBookService orderBookService;

    @Autowired
    private EventMessageConverter eventMessageConverter;

    @Autowired
    private TransactionService transactionService;

    void clearAll() {
        this.transactionService.clear();
        this.orderBookService.clear();
    }

    void checkIfTransactionListIsEmpty() {
        assertTrue(this.transactionService.build().isEmpty());
    }

    void checkIfTransactionMatches(Transaction transaction, TransactionParams transactionParams) {
        assertEquals(transaction.getProduct(), transactionParams.getProduct());
        assertEquals(String.valueOf(transaction.getAmount()), String.valueOf(transactionParams.getAmount()));
        assertEquals(String.valueOf(transaction.getPrice()), String.valueOf(transactionParams.getPrice()));
        assertEquals(transaction.getBrokerBuy(), transactionParams.getBrokerBuy());
        assertEquals(transaction.getBrokerSell(), transactionParams.getBrokerSell());
        assertEquals(transaction.getClientBuy(), transactionParams.getClientBuy());
        assertEquals(transaction.getClientSell(), transactionParams.getClientSell());
    }

    void checkIfOrderListIsEmpty(OrderList orderList) {
        assertTrue(orderList.entries().isEmpty());
    }

    void checkIfOrderMatches(OrderEntry orderEntry, OrderParams orderParams) {
        assertEquals(String.valueOf(orderEntry.getTimestamp()), String.valueOf(orderParams.getTimestamp()));
        assertEquals(String.valueOf(orderEntry.getAmount()), String.valueOf(orderParams.getAmount()));
        assertEquals(String.valueOf(orderEntry.getPrice()), String.valueOf(orderParams.getPrice()));
        assertEquals(orderEntry.getBroker(), orderParams.getBroker());
        assertEquals(orderEntry.getClient(), orderParams.getClient());
    }

    Set<Transaction> getTransaction() {
        return this.transactionService.build();
    }

    Transaction getTransactionByBrokerAndId(Set<Transaction> transactions, String product, Integer transactionId) {
        Optional<Transaction> first = transactions.parallelStream()
                .filter(transaction ->
                        transaction.getProduct().equals(product) && transaction.getId() == transactionId).findFirst();
        if (first.isPresent())
            return first.get();
        return null;
    }

    OrderEntry getBuyEntry(String product, int orderId) {
        return this.orderBookService.getBook(product).getBuyEntries().findById(orderId);
    }

    OrderList getBuyEntries(String product) {
        return this.orderBookService.getBook(product).getBuyEntries();
    }

    OrderEntry getSellEntry(String product, int orderId) {
        return this.orderBookService.getBook(product).getSellEntries().findById(orderId);
    }

    OrderList getSellEntries(String product) {
        return this.orderBookService.getBook(product).getSellEntries();
    }

    void injectBuyOrder(String product, OrderParams orderParams) {
        this.orderBookService.getBook(product).getBuyEntries().add(createOrderEntry(orderParams));
    }

    void injectSellOrder(String product, OrderParams orderParams) {
        this.orderBookService.getBook(product).getSellEntries().add(createOrderEntry(orderParams));
    }

    void addNewOrder(OrderParams orderParams) {
        this.eventRouter.route(this.eventMessageConverter.convert(createNewOrder(orderParams)));
    }

    private BrokerMessage createNewOrder(OrderParams orderParams) {
        com.gft.digitalbank.exchange.model.orders.BrokerMessage brokerMessage;
        switch (orderParams.getType()) {
            case ORDER:
                brokerMessage = createPositionOrder(orderParams);
                break;
            case MODIFICATION:
                brokerMessage = createModificationOrder(orderParams);
                break;
            case CANCEL:
                brokerMessage = createCancellationOrder(orderParams);
                break;
            default:
                throw new InvalidBrokerMessageException(MESSAGE_TYPE_NOT_SUPPORTED);
        }
        return brokerMessage;
    }

    private PositionOrder createPositionOrder(OrderParams orderParams) {
        return PositionOrder.builder()
                .id(orderParams.getOrderId())
                .timestamp(orderParams.getTimestamp())
                .product(orderParams.getProduct())
                .side(orderParams.getSide())
                .details(OrderDetails.builder()
                        .amount(orderParams.getAmount())
                        .price(orderParams.getPrice())
                        .build())
                .broker(orderParams.getBroker())
                .client(orderParams.getClient())
                .build();
    }

    private BrokerMessage createCancellationOrder(OrderParams orderParams) {
        return CancellationOrder.builder()
                .id(orderParams.getOrderId())
                .timestamp(orderParams.getTimestamp())
                .messageType(orderParams.getType())
                .cancelledOrderId(orderParams.getCancelledOrderId())
                .broker(orderParams.getBroker())
                .build();
    }

    private BrokerMessage createModificationOrder(OrderParams orderParams) {
        return com.gft.digitalbank.exchange.model.orders.ModificationOrder.builder()
                .id(orderParams.getOrderId())
                .timestamp(orderParams.getTimestamp())
                .modifiedOrderId(orderParams.getModifiedOrderId())
                .details(OrderDetails.builder()
                        .amount(orderParams.getAmount())
                        .price(orderParams.getPrice())
                        .build())
                .broker(orderParams.getBroker())
                .build();
    }

    private OrderEntry createOrderEntry(OrderParams orderParams) {
        return OrderEntry.builder()
                .id(orderParams.getOrderId())
                .timestamp(orderParams.getTimestamp())
                .price(orderParams.getPrice())
                .amount(orderParams.getAmount())
                .client(orderParams.getClient())
                .broker(orderParams.getBroker())
                .build();
    }
}
