package com.gft.digitalbank.exchange.solution.unittest.converters;

import com.gft.digitalbank.exchange.model.orders.CancellationOrder;
import com.gft.digitalbank.exchange.solution.converters.BrokerMessageConverter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Kemal_Demir on 12/7/2016.
 */
public class CancellationOrderConverterTest {

    @DataProvider
    public Object[][] validCancellationOrder() {
        return BrokerMessageDataProvider.getValidCancellationOrder();
    }

    @Test(dataProvider = "validCancellationOrder")
    public void shouldCancellationOrderHaveCorrectStructure(String jsonFormat) {
        // Given
        BrokerMessageConverter brokerMessageConverter = new BrokerMessageConverter();

        // When
        CancellationOrder actual = (CancellationOrder) brokerMessageConverter.convert(jsonFormat);

        // Then
        assertNotNull(actual.getMessageType());
        assertNotNull(actual.getId());
        assertNotNull(actual.getTimestamp());
        assertNotNull(actual.getBroker());
        assertNotNull(actual.getCancelledOrderId());
    }
}
