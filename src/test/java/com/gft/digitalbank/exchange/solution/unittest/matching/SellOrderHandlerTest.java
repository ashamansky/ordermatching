package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.solution.matching.*;
import com.gft.digitalbank.exchange.solution.matching.handlers.SellOrderHandler;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMapper;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderEntryStubber;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderRegistryStubs;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class SellOrderHandlerTest {

    @Mock
    private OrderBookService orderBookService;

    @Mock
    private TransactionService transactionService;

    @Mock
    private OrderMapper orderMapper;

    @InjectMocks
    private SellOrderHandler orderHandler;

    private final OrderEntryStubber stub = new OrderEntryStubber();


    @BeforeMethod
    public void setUp() {
        this.orderHandler = new SellOrderHandler();
        initMocks(this);
    }

    @Test(dataProvider = "orderRegistryStubs", dataProviderClass = OrderRegistryStubs.class)
    public void noMatchGivesNoTransaction(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        OrderRegistry orderRegistry = registry.get("ABC");
        orderHandler.handle(stub.of(1, 1), orderRegistry.getBuyEntries(), orderRegistry.getSellEntries());

        verify(transactionService, never()).log(anyString(), any(OrderEntry.class), any(OrderEntry.class));
    }

    @Test(dataProvider = "orderRegistryStubs", dataProviderClass = OrderRegistryStubs.class)
    public void noMatchAddsOrder(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        OrderRegistry registryStub = registry.get("ABC");
        OrderList sellEntries = mock(OrderList.class);

        OrderEntry orderEntry = stub.of(1, 10000);
        orderHandler.handle(orderEntry, registryStub.getBuyEntries(), sellEntries);

        verify(sellEntries, times(1)).add(orderEntry);
    }

    @DataProvider
    public static Object[][] fullyMatchingOrderPairs() {
        final OrderEntryStubber stub = new OrderEntryStubber();
        final List<OrderEntry> details = Arrays.asList(
                stub.of(5, 300),
                stub.of(50, 400),
                stub.of(1, 1)
        );

        List<Object[]> cases = details.stream()
                .map(entry -> new Object[]{entry, entry})
                .collect(Collectors.toList());

        return cases.toArray(new Object[cases.size()][]);
    }

    @Test(dataProvider = "fullyMatchingOrderPairs")
    public void fullMatchAddsTransaction(OrderEntry order, OrderEntry matchingEntry) {
        OrderList buyEntries = mock(OrderList.class);

        when(buyEntries.findByPriceDownTo(order.getPrice()))
                .thenReturn(Collections.singletonList(matchingEntry));

        assertEquals(orderHandler.handle(order, buyEntries, mock(OrderList.class)), Collections.singletonList(matchingEntry));
    }

    @DataProvider
    public static Object[][] sellOrdersFulfillingWholeBuyOrders() {
        final OrderEntryStubber stub = new OrderEntryStubber();

        return new OrderEntry[][]{
                {
                        stub.of(100, 100),  // Sell order
                        stub.of(300, 100),  // Buy order
                        stub.of(200, 100),  // Expected remainder of buy order left in order list
                        stub.of(100, 100)   // Expected match
                },
                {
                        stub.of(1, 10),   // Sell order
                        stub.of(30, 10),  // Buy order
                        stub.of(29, 10),  // Expected remainder of buy order left in order list
                        stub.of(1, 10)    // Expected match
                },
                {
                        stub.of(100, 120),  // Sell order
                        stub.of(3000, 120), // Buy order
                        stub.of(2900, 120), // Expected remainder of buy order left in order list
                        stub.of(100, 120)   // Expected match
                },
                {
                        stub.of(100, 120),  // Sell order
                        stub.of(3000, 140), // Buy order
                        stub.of(2900, 140), // Expected remainder of buy order left in order list
                        stub.of(100, 140)   // Expected match
                }
        };
    }

    @Test(dataProvider = "sellOrdersFulfillingWholeBuyOrders")
    public void fulfilledSellOrderUpdatesBuyOrder(OrderEntry order, OrderEntry matchingEntry, OrderEntry expectedEntry, OrderEntry match) {
        OrderList buyEntries = mock(OrderList.class);
        ArgumentCaptor<OrderEntry> buyOrderNew = ArgumentCaptor.forClass(OrderEntry.class);

        when(buyEntries.findByPriceDownTo(order.getPrice()))
                .thenReturn(Collections.singletonList(matchingEntry));

        orderHandler.handle(order, buyEntries, mock(OrderList.class));

        verify(buyEntries, times(1)).add(buyOrderNew.capture());
        assertEquals(buyOrderNew.getValue().getAmount(), expectedEntry.getAmount());
        assertEquals(buyOrderNew.getValue().getPrice(), expectedEntry.getPrice());
    }

    @Test(dataProvider = "sellOrdersFulfillingWholeBuyOrders")
    public void fulfilledSellOrderAddTransactionWithMatchedAmountAndPrice(OrderEntry order, OrderEntry matchingEntry, OrderEntry expectedEntry, OrderEntry match) {
        OrderList buyEntries = mock(OrderList.class);

        when(buyEntries.findByPriceDownTo(order.getPrice()))
                .thenReturn(Collections.singletonList(matchingEntry));

        Collection<OrderEntry> matches = orderHandler.handle(order, buyEntries, mock(OrderList.class));

        assertEquals(matches, Collections.singletonList(match));
    }

    @Test(dataProvider = "sellOrdersFulfillingWholeBuyOrders")
    public void fulfilledSellOrderAddsTransaction(OrderEntry sellOrder, OrderEntry matchingEntry, OrderEntry expectedEntry, OrderEntry match) {
        OrderList buyEntries = mock(OrderList.class);

        when(buyEntries.findByPriceDownTo(sellOrder.getPrice()))
                .thenReturn(Collections.singletonList(matchingEntry));

        assertEquals(orderHandler.handle(sellOrder, buyEntries, mock(OrderList.class)), Collections.singletonList(match));
    }

    @DataProvider
    public static Object[][] multipleBuyOrdersFulfillingWholeSellOrders() {
        final OrderEntryStubber stub = new OrderEntryStubber();
        // Orders are taken from right to left
        final OrderEntry[][][] details = {
                {
                        {stub.of(100, 100)},  // Sell order
                        {stub.of(50, 100), stub.of(50, 100)},  // Buy orders
                        {stub.of(50, 100), stub.of(50, 100)},   // Expected removed orders
                        {} // Expected added sell orders
                },
                {
                        {stub.of(100, 10)},   // Sell order
                        {stub.of(40, 10), stub.of(20, 10), stub.of(50, 10)},  // Buy orders
                        {stub.of(30, 10), stub.of(20, 10), stub.of(50, 10)},  // Expected buy orders removed
                        {stub.of(10, 10)} // Expected added sell orders
                },
                {
                        {stub.of(100, 120)},  // Sell order
                        {stub.of(50, 121), stub.of(50, 122)}, // Buy orders
                        {stub.of(50, 121), stub.of(50, 122)}, // Expected buy orders removed
                        {} // Expected added sell orders
                },
                {
                        {stub.of(10, 1200)},  // Sell order
                        {stub.of(15, 1200)}, // Buy orders
                        {stub.of(10, 1200)}, // Expected buy orders removed
                        {stub.of(5, 1200)} // Expected added buy orders
                },
                {
                        {stub.of(50, 200)},  // Sell order
                        {stub.of(40, 200), stub.of(10, 200)}, // Buy orders
                        {stub.of(40, 200), stub.of(10, 200)}, // Expected buy orders removed
                        {} // Expected added sell orders
                },
                {
                        {stub.of(50, 200)},  // Sell order
                        {stub.of(40, 200), stub.of(10, 300)}, // Buy orders
                        {stub.of(40, 200), stub.of(10, 300)}, // Expected buy orders removed
                        {} // Expected added sell orders
                }
        };
        for (OrderEntry[][] testCase : details) {
            for (OrderEntry[] testArgument : testCase) {
                Collections.reverse(Arrays.asList(testArgument));
            }
        }

        return details;
    }

    @Test(dataProvider = "multipleBuyOrdersFulfillingWholeSellOrders")
    public void lastPartialMatchAreUpdated(OrderEntry[] order, OrderEntry[] buyOrders, OrderEntry[] expectedRemovals, OrderEntry[] expectedAdditions) {
        OrderList buyEntries = mock(OrderList.class);
        ArgumentCaptor<OrderEntry> added = ArgumentCaptor.forClass(OrderEntry.class);

        when(buyEntries.findByPriceDownTo(order[0].getPrice()))
                .thenReturn(Arrays.asList(buyOrders));

        orderHandler.handle(order[0], buyEntries, mock(OrderList.class));

        verify(buyEntries, times(expectedAdditions.length)).add(added.capture());
        assertEquals(added.getAllValues(), Arrays.asList(expectedAdditions));
    }

    @Test(dataProvider = "multipleBuyOrdersFulfillingWholeSellOrders")
    public void sellOrderAddsTransactionPerMatch(OrderEntry[] order, OrderEntry[] buyOrders, OrderEntry[] expectedRemovals, OrderEntry[] expectedAdditions) {
        OrderList buyEntries = mock(OrderList.class);

        when(buyEntries.findByPriceDownTo(order[0].getPrice()))
                .thenReturn(Arrays.asList(buyOrders));

        assertEquals(orderHandler.handle(order[0], buyEntries, mock(OrderList.class)), Arrays.asList(expectedRemovals));
    }

}