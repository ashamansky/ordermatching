package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.matching.impl.LazyTransactionService;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.matching.sequence.TransactionIDCreator;
import com.gft.digitalbank.exchange.solution.matching.TransactionMapper;
import com.gft.digitalbank.exchange.solution.models.LogEntry;
import com.gft.digitalbank.exchange.solution.repository.TransactionRepository;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderEntryStubber;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;

public class LazyTransactionServiceTest{
    private static final String BROKER_1 = "broker1";
    private static final String CLIENT_1 = "client1";
    private static final String BROKER_2 = "broker2";
    private static final String CLIENT_2 = "client2";
    private static final String BROKER_3 = "broker3";
    private static final String CLIENT_3 = "client3";

    @Mock
    private TransactionMapper transactionMapper;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private TransactionIDCreator transactionIDCreator;

    @InjectMocks
    private LazyTransactionService transactionService;

    @BeforeMethod
    public void setUp() {
        transactionService = new LazyTransactionService();
        initMocks(this);
    }

    @DataProvider
    public static Object[][] logEntriesExamplesProvider() {
        return new Object[][]{
                {
                        OrderEntry.builder().id(1).amount(10).price(15).client("c1").timestamp(1000L).broker("b1").build(),
                        OrderEntry.builder().id(2).amount(10).price(15).client("c6").timestamp(1111L).broker("b7").build(),
                        Transaction.builder().product("ABC").id(1).amount(10).price(15).clientBuy("c1").clientSell("c6").brokerBuy("b1").brokerSell("b7").build()
                },
                {
                        OrderEntry.builder().id(3).amount(200).price(9000).client("c8").timestamp(1232L).broker("b4").build(),
                        OrderEntry.builder().id(4).amount(200).price(9000).client("c3").timestamp(4961L).broker("b4").build(),
                        Transaction.builder().product("DEF").id(3).amount(200).price(9000).clientBuy("c8").clientSell("c3").brokerBuy("b4").brokerSell("b4").build()
                }
        };
    }

    @Test(dataProvider = "logEntriesExamplesProvider")
    public void testLog(OrderEntry buyEntry, OrderEntry sellEntry, Transaction expectedTransaction) {
        LogEntry logEntry = new LogEntry(expectedTransaction.getId(), expectedTransaction.getProduct(), buyEntry, sellEntry);
        when(transactionRepository.findAll()).thenReturn(Collections.singletonList(logEntry));
        when(transactionMapper.createTransaction(logEntry))
                .thenReturn(expectedTransaction);

        transactionService.log(expectedTransaction.getProduct(), buyEntry, sellEntry);

        boolean result = transactionService.build().contains(expectedTransaction);
        verify(transactionMapper).createTransaction(any(LogEntry.class));


        assertTrue(result);

    }

    @DataProvider
    public static Object[][] logExamplesProvider() {
        final OrderEntryStubber stub = new OrderEntryStubber();

        final ArrayList<Object[][]> testCases = new ArrayList<>();

        testCases.add(new Object[][]{
                {
                        new Object[]{"ABC", stub.of(50, 100), stub.of(50, 100)},
                        new Object[]{"DEF", stub.of(20, 100), stub.of(20, 100)}
                },
                {
                        Transaction.builder().id(1).amount(50).price(100).product("ABC").brokerBuy(BROKER_1).brokerSell(BROKER_1).clientBuy(CLIENT_1).clientSell(CLIENT_1).build(),
                        Transaction.builder().id(1).amount(20).price(100).product("DEF").brokerBuy(BROKER_1).brokerSell(BROKER_1).clientBuy(CLIENT_1).clientSell(CLIENT_1).build()
                }
        });

        stub.inc();
        testCases.add(new Object[][]{
                {
                        new Object[]{"ABC", stub.of(30, 10), stub.of(30, 10)},
                        new Object[]{"DEF", stub.of(19, 10), stub.of(20, 10)},
                        new Object[]{"XYZ", stub.of(50, 10), stub.of(50, 10)}
                },
                {
                        Transaction.builder().id(2).price(10).amount(30).product("ABC").brokerBuy(BROKER_2).brokerSell(BROKER_2).clientBuy(CLIENT_2).clientSell(CLIENT_2).build(),
                        Transaction.builder().id(2).price(10).amount(20).product("DEF").brokerBuy(BROKER_2).brokerSell(BROKER_2).clientBuy(CLIENT_2).clientSell(CLIENT_2).build(),
                        Transaction.builder().id(2).price(10).amount(50).product("XYZ").brokerBuy(BROKER_2).brokerSell(BROKER_2).clientBuy(CLIENT_2).clientSell(CLIENT_2).build()
                }
        });

        stub.inc();
        testCases.add(new Object[][]{
                {
                        new Object[]{"ABC", stub.of(50, 10), stub.of(50, 10)},
                        new Object[]{"DEF", stub.of(50, 110), stub.of(50, 110)},
                        new Object[]{"XYZ", stub.of(100, 121), stub.of(100, 121)},
                        new Object[]{"DBC", stub.of(100, 122), stub.of(100, 122)}
                },
                {
                        Transaction.builder().id(3).price(122).amount(100).product("ABC").brokerBuy(BROKER_3).brokerSell(BROKER_3).clientBuy(CLIENT_3).clientSell(CLIENT_3).build(),
                        Transaction.builder().id(3).price(121).amount(100).product("DEF").brokerBuy(BROKER_3).brokerSell(BROKER_3).clientBuy(CLIENT_3).clientSell(CLIENT_3).build(),
                        Transaction.builder().id(3).price(110).amount(50).product("XYZ").brokerBuy(BROKER_3).brokerSell(BROKER_3).clientBuy(CLIENT_3).clientSell(CLIENT_3).build(),
                        Transaction.builder().id(3).price(10).amount(50).product("DBC").brokerBuy(BROKER_3).brokerSell(BROKER_3).clientBuy(CLIENT_3).clientSell(CLIENT_3).build()
                }
        });
        return testCases.toArray(new Object[testCases.size()][]);
    }

}