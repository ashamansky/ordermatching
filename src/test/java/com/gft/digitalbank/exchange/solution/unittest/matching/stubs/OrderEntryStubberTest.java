package com.gft.digitalbank.exchange.solution.unittest.matching.stubs;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class OrderEntryStubberTest {

    private OrderEntryStubber stubber;
    private OrderEntry stub1;
    private OrderEntry stub2;

    @BeforeMethod
    private void setUp() {
        this.stubber = new OrderEntryStubber();
        stub1 = stubber.of(1,1);
        stub2 = stubber.inc().of(1,1);
    }

    @Test
    public void incIncreasesId() {
        Assert.assertTrue(stub1.getId() < stub2.getId(), "Id not monotonic");
    }

    @Test
    public void incIncreasesTimestamp() {
        Assert.assertTrue(stub1.getTimestamp() < stub2.getTimestamp(), "Id not monotonic");
    }

    @Test
    public void resetsId() {
        OrderEntry stub3 = stubber.reset().of(1,1);
        Assert.assertEquals(stub1.getId(), stub3.getId());
    }

    @Test
    public void resetsTimestamp() {
        OrderEntry stub3 = stubber.reset().of(1,1);
        Assert.assertEquals(stub1.getTimestamp(), stub3.getTimestamp());
    }
}