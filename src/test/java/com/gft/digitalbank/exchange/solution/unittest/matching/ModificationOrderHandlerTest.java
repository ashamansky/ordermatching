package com.gft.digitalbank.exchange.solution.unittest.matching;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.solution.events.BuyEvent;
import com.gft.digitalbank.exchange.solution.events.ModificationEvent;
import com.gft.digitalbank.exchange.solution.events.SellEvent;
import com.gft.digitalbank.exchange.solution.matching.*;
import com.gft.digitalbank.exchange.solution.matching.handlers.BuyOrderHandler;
import com.gft.digitalbank.exchange.solution.matching.handlers.ModificationOrderHandler;
import com.gft.digitalbank.exchange.solution.matching.handlers.SellOrderHandler;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.EventStubber;
import com.gft.digitalbank.exchange.solution.unittest.matching.stubs.OrderRegistryStubs;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class ModificationOrderHandlerTest {

    private static final String BROKER1 = "broker1";
    private static final String PRODUCT_ABC = "ABC";
    private EventStubber stub;

    @Mock
    private OrderBookService orderBookService;

    @Mock
    private BuyOrderHandler buyOrderHandler;

    @Mock
    private SellOrderHandler sellOrderHandler;

    @InjectMocks
    private OrderHandler<ModificationEvent> modificationOrderHandler = new ModificationOrderHandler();

    @BeforeMethod
    public void setUp() {
        this.stub = new EventStubber();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldNotModifyOrderWhenNoOrderBookFound() {
        // Given
        String broker = BROKER1;
        when(this.orderBookService.findBookByBrokerAndId(broker, 1)).thenReturn(null);

        // When
        this.modificationOrderHandler.handle(this.stub.ofModificationEvent(7, 1, 100, 300, broker));

        // Then
        verify(this.orderBookService).findBookByBrokerAndId(broker, 1);
    }

    @Test(dataProvider = "emptyOrderRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldNotModifyOrderWhenOrderBookEmpty(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        String broker = BROKER1;
        when(this.orderBookService.findBookByBrokerAndId(broker, 1)).thenReturn(registry.get(PRODUCT_ABC));

        // When
        this.modificationOrderHandler.handle(this.stub.ofModificationEvent(7, 1, 100, 300, broker));

        // Then
        verify(this.orderBookService).findBookByBrokerAndId(broker, 1);
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldNotModifyOrderWhenNoOrderFound(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        String broker = BROKER1;
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get(PRODUCT_ABC));

        // When
        this.modificationOrderHandler.handle(this.stub.ofModificationEvent(7, 9, 100, 300, broker));

        // Then
        assertEquals(registry.get(PRODUCT_ABC).getBuyEntries().entries().size(), 3);
        assertEquals(registry.get(PRODUCT_ABC).getSellEntries().entries().size(), 3);
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldModifyBuyOrderWhenOrderFound(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        String broker = BROKER1;
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get(PRODUCT_ABC));

        // When
        this.modificationOrderHandler.handle(this.stub.ofModificationEvent(7, 1, 100, 300, broker));

        // Then
        verify(buyOrderHandler).handle(any(BuyEvent.class));
        Assert.assertEquals(2, registry.get(PRODUCT_ABC).getBuyEntries().entries().size());
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldNotModifyBuyOrderWhenBrokerNotMatch(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        String broker = "broker2";
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get(PRODUCT_ABC));

        // When
        this.modificationOrderHandler.handle(this.stub.ofModificationEvent(7, 1, 100, 300, broker));

        // Then
        Assert.assertEquals(3, registry.get(PRODUCT_ABC).getBuyEntries().entries().size());
        Assert.assertNull(registry.get(PRODUCT_ABC).getBuyEntries().findById(7));
        Assert.assertNotNull(registry.get(PRODUCT_ABC).getBuyEntries().findById(1));
        Assert.assertEquals(3, registry.get(PRODUCT_ABC).getSellEntries().entries().size());
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldModifySellOrderWhenOrderFound(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        String broker = "broker4";
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get(PRODUCT_ABC));

        // When
        this.modificationOrderHandler.handle(this.stub.ofModificationEvent(7, 4, 100, 300, broker));

        verify(sellOrderHandler).handle(any(SellEvent.class));

        // Then
        Assert.assertEquals(2, registry.get(PRODUCT_ABC).getSellEntries().entries().size());
    }

    @Test(dataProvider = "filledRegistry", dataProviderClass = OrderRegistryStubs.class)
    public void shouldNotModifySellOrderWhenBrokerNotMatch(Map<String, OrderRegistry> registry, Set<OrderBook> books) {
        // Given
        String broker = BROKER1;
        when(this.orderBookService.findBookByBrokerAndId(anyString(), anyInt())).thenReturn(registry.get(PRODUCT_ABC));

        // When
        this.modificationOrderHandler.handle(this.stub.ofModificationEvent(7, 4, 100, 300, broker));

        verifyZeroInteractions(sellOrderHandler);

        // Then
        Assert.assertEquals(3, registry.get(PRODUCT_ABC).getSellEntries().entries().size());

    }
}
