package com.gft.digitalbank.exchange.solution.behave;

import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.solution.configuration.StockExchangeConfig;
import com.gft.digitalbank.exchange.solution.processors.impl.MatchingProcessor;
import com.gft.digitalbank.exchange.solution.repository.BrokerRepository;
import com.gft.digitalbank.exchange.solution.repository.impl.BrokerRepositoryImpl;
import com.gft.digitalbank.exchange.solution.routers.EventRouter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

import java.util.Collections;

@Configuration
@Slf4j
@ComponentScan(basePackages = {"com.gft.digitalbank.exchange.solution.*"},
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = {StockExchangeConfig.class}))
public class AcceptanceTestConfiguration {

    @Autowired
    private MatchingProcessor matchingProcessor;

    @Bean
    public EventRouter eventRouter() {
        EventRouter eventRouter = new EventRouter();
        eventRouter.register(matchingProcessor);
        return eventRouter;
    }

    @Bean
    public ProcessingListener processingListener() {
        return solutionResult -> log.trace("Submitted result: {}", solutionResult);
    }

    @Bean
    public BrokerRepository brokerRepository() {
        return new BrokerRepositoryImpl(Collections.singletonList("default-broker"));
    }

}

