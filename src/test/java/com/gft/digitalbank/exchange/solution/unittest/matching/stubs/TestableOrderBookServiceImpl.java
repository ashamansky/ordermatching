package com.gft.digitalbank.exchange.solution.unittest.matching.stubs;

import com.gft.digitalbank.exchange.solution.matching.impl.OrderBookServiceImpl;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
@Profile("test")
public class TestableOrderBookServiceImpl extends OrderBookServiceImpl {

    @Autowired
    private OrderRepository orderRepository;

    public void setRegistries(Map<String, OrderRegistry> registries) {
        orderRepository.initRegistries(registries);
    }
}
