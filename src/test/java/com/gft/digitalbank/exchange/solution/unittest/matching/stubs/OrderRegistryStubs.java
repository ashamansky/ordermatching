package com.gft.digitalbank.exchange.solution.unittest.matching.stubs;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.solution.matching.*;
import com.gft.digitalbank.exchange.solution.matching.impl.OrderRegistryImpl;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import lombok.val;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Collections;


public class OrderRegistryStubs {
    @DataProvider
    public static Object[][] emptyOrderRegistry() {
        String product = "ABC";
        OrderRegistry registry = new OrderRegistryImpl(product);

        OrderBook book = OrderBook.builder().product(product).build();
        return new Object[][]{{ImmutableMap.of(product, registry), Sets.newHashSet(book)}};
    }

    @DataProvider
    public static Object[][] filledRegistry() {
        String product = "ABC";
        OrderRegistry registry = new OrderRegistryImpl(product);

        val bookBuilder = OrderBook.builder().product(product);
        val entryBuilder = com.gft.digitalbank.exchange.model.OrderEntry.builder();

        registry.getBuyEntries().add(OrderEntry.builder().id(1).timestamp(1L).amount(1).price(100).broker("broker1").client("client1").build());
        registry.getBuyEntries().add(OrderEntry.builder().id(3).timestamp(1L).amount(100).price(1).broker("broker3").client("client3").build());
        registry.getBuyEntries().add(OrderEntry.builder().id(2).timestamp(1L).amount(10).price(10).broker("broker2").client("client2").build());
        registry.getSellEntries().add(OrderEntry.builder().id(4).timestamp(2L).amount(5).price(300).broker("broker4").client("client1").build());
        registry.getSellEntries().add(OrderEntry.builder().id(6).timestamp(3L).amount(50).price(270).broker("broker7").client("client3").build());
        registry.getSellEntries().add(OrderEntry.builder().id(5).timestamp(2L).amount(25).price(900).broker("broker5").client("client2").build());

        bookBuilder.buyEntry(entryBuilder.id(1).amount(1).price(100).broker("broker1").client("client1").build());
        bookBuilder.buyEntry(entryBuilder.id(2).amount(10).price(10).broker("broker2").client("client2").build());
        bookBuilder.buyEntry(entryBuilder.id(3).amount(100).price(1).broker("broker3").client("client3").build());
        bookBuilder.sellEntry(entryBuilder.id(4).amount(5).price(300).broker("broker4").client("client1").build());
        bookBuilder.sellEntry(entryBuilder.id(5).amount(25).price(900).broker("broker5").client("client2").build());
        bookBuilder.sellEntry(entryBuilder.id(6).amount(50).price(270).broker("broker7").client("client3").build());

        return new Object[][]{{ImmutableMap.of(product, registry), Sets.newHashSet(bookBuilder.build())}};
    }

    @DataProvider
    public static Object[][] multipleSecuritiesRegistry() {
        val book1Builder = OrderBook.builder();
        val book2Builder = OrderBook.builder();
        val entryBuilder = com.gft.digitalbank.exchange.model.OrderEntry.builder();
        String product1 = "ABC";
        String product2 = "DEF";
        OrderRegistry registry1 = new OrderRegistryImpl(product1);
        OrderRegistry registry2 = new OrderRegistryImpl(product2);

        registry1.getBuyEntries().add(OrderEntry.builder().id(1).timestamp(1L).amount(1).price(100).broker("broker1").client("client1").build());
        registry1.getBuyEntries().add(OrderEntry.builder().id(3).timestamp(1L).amount(100).price(1).broker("broker3").client("client3").build());
        registry1.getBuyEntries().add(OrderEntry.builder().id(2).timestamp(1L).amount(10).price(10).broker("broker2").client("client2").build());

        registry1.getSellEntries().add(OrderEntry.builder().id(4).timestamp(2L).amount(5).price(300).broker("broker4").client("client1").build());
        registry1.getSellEntries().add(OrderEntry.builder().id(6).timestamp(3L).amount(50).price(270).broker("broker7").client("client3").build());
        registry1.getSellEntries().add(OrderEntry.builder().id(5).timestamp(2L).amount(25).price(900).broker("broker5").client("client2").build());

        book1Builder.buyEntry(entryBuilder.id(1).amount(1).price(100).broker("broker1").client("client1").build());
        book1Builder.buyEntry(entryBuilder.id(2).amount(10).price(10).broker("broker2").client("client2").build());
        book1Builder.buyEntry(entryBuilder.id(3).amount(100).price(1).broker("broker3").client("client3").build());
        book1Builder.sellEntry(entryBuilder.id(4).amount(5).price(300).broker("broker4").client("client1").build());
        book1Builder.sellEntry(entryBuilder.id(5).amount(25).price(900).broker("broker5").client("client2").build());
        book1Builder.sellEntry(entryBuilder.id(6).amount(50).price(270).broker("broker7").client("client3").build());
        val book1 = book1Builder.product(product1).build();

        registry2.getBuyEntries().add(OrderEntry.builder().id(7).timestamp(10L).amount(13).price(123).broker("broker1").client("client1").build());
        registry2.getSellEntries().add(OrderEntry.builder().id(8).timestamp(20L).amount(52).price(321).broker("broker4").client("client1").build());

        book2Builder.buyEntry(entryBuilder.id(7).amount(13).price(123).broker("broker1").client("client1").build());
        book2Builder.sellEntry(entryBuilder.id(8).amount(52).price(321).broker("broker4").client("client1").build());
        val book2 = book2Builder.product(product2).build();

        return new Object[][]{{
                ImmutableMap.of(product1, registry1, product2, registry2),
                Sets.newHashSet(book1, book2)}};
    }

    @DataProvider
    public static Object[][] orderRegistryStubs() {
        ArrayList<Object[]> testCases = new ArrayList<>();
//        Collections.addAll(testCases, emptyOrderRegistry());
        Collections.addAll(testCases, filledRegistry());
        Collections.addAll(testCases, multipleSecuritiesRegistry());
        return testCases.toArray(new Object[testCases.size()][]);
    }
}
