package com.gft.digitalbank.exchange.solution.unittest.repository;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.matching.impl.OrderRegistryImpl;
import com.gft.digitalbank.exchange.solution.repository.OrderRepository;
import com.gft.digitalbank.exchange.solution.repository.impl.OrderRepositoryImpl;
import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class OrderRepositoryTest {

    private static final String PRODUCT = "ABC";
    private static final String BROKER_1 = "b1";
    private static final String BROKER_2 = "b2";
    private OrderRepository orderRepository = new OrderRepositoryImpl();

    private OrderEntry buyEntry;
    private OrderEntry sellEntry;

    @BeforeMethod
    public void setUp() {
        Map<String, OrderRegistry> registryMap = new ConcurrentHashMap<>();
        registryMap.put(PRODUCT,new OrderRegistryImpl(PRODUCT));

        buyEntry = OrderEntry.builder()
                .id(1)
                .amount(100)
                .price(10)
                .timestamp(1)
                .client("c100")
                .broker(BROKER_1).build();
        sellEntry = OrderEntry.builder()
                .id(1)
                .amount(100)
                .price(10)
                .timestamp(2)
                .client("c101")
                .broker(BROKER_2).build();

        registryMap.get(PRODUCT).getBuyEntries().add(buyEntry);
        registryMap.get(PRODUCT).getSellEntries().add(sellEntry);

        orderRepository.initRegistries(registryMap);
    }

    @Test
    public void testAddOrder() {
        OrderRegistry orderRegistry = orderRepository.findOrderBookByProduct(PRODUCT);
        Assert.assertNotNull(orderRegistry);

        Assert.assertEquals(PRODUCT, orderRegistry.getProduct());
        Assert.assertFalse(orderRegistry.isEmpty());
    }

    @Test
    public void tesRemoveByBrokerAndId() {
        orderRepository.removeByBrokerAndId(BROKER_1, 1);
        Assert.assertNull(orderRepository.findOrderBookByBrokerAndId(BROKER_1, 1));
    }

    @Test
    public void testFindAllOrderRegistry() {
        Collection<OrderRegistry> orders = orderRepository.findAllOrders();
        Assert.assertEquals(1, orders.size());
    }

    @AfterMethod
    public void tearDown() {
        orderRepository.removeAll();

    }
}
