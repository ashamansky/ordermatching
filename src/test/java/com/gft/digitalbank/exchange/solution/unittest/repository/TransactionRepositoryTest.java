package com.gft.digitalbank.exchange.solution.unittest.repository;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.models.LogEntry;
import com.gft.digitalbank.exchange.solution.repository.TransactionRepository;
import com.gft.digitalbank.exchange.solution.repository.impl.TransactionRepositoryImpl;
import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;


public class TransactionRepositoryTest {

    private static final String PRODUCT = "a";
    private TransactionRepository transactionRepository = new TransactionRepositoryImpl();
    private OrderEntry buyEntry;
    private OrderEntry sellEntry;

    @BeforeMethod
    public void setUp() {
        buyEntry = OrderEntry.builder()
                .id(1)
                .amount(100)
                .price(10)
                .timestamp(1)
                .client("c100")
                .broker("b1").build();
        sellEntry = OrderEntry.builder()
                .id(1)
                .amount(100)
                .price(10)
                .timestamp(2)
                .client("c101")
                .broker("b2").build();

        List<LogEntry> logEntries = transactionRepository.add(new LogEntry(1, PRODUCT, buyEntry, sellEntry));
        Assert.assertEquals(1, logEntries.size());

    }

    @Test
    public void testAddTransactions() {
        List<LogEntry> logEntries = transactionRepository.findAll();

        Assert.assertEquals(1, logEntries.size());

        Assert.assertEquals(buyEntry, logEntries.get(0).getBuyEntry());
        Assert.assertEquals(sellEntry, logEntries.get(0).getSellEntry());
        Assert.assertEquals("a", logEntries.get(0).getProduct());
    }

    @Test
    public void testRemoveAll() {
        transactionRepository.removeAll();
        Assert.assertTrue(transactionRepository.findAll().isEmpty());
    }

    @AfterMethod
    public void tearDown() {
        transactionRepository.removeAll();

    }
}
