package com.gft.digitalbank.exchange.solution.behave.stories.params;

import com.gft.digitalbank.exchange.model.orders.MessageType;
import com.gft.digitalbank.exchange.model.orders.Side;
import org.jbehave.core.annotations.AsParameters;
import org.jbehave.core.annotations.Parameter;

/**
 * Created by Kemal_Demir on 15/7/2016.
 */
@AsParameters
public final class OrderParams {
    @Parameter(name = "order_id")
    private Integer orderId;

    @Parameter(name = "modifiedOrderId")
    private Integer modifiedOrderId;

    @Parameter(name = "cancelledOrderId")
    private Integer cancelledOrderId;

    @Parameter(name = "timestamp")
    private Long timestamp;

    @Parameter(name = "type")
    private MessageType type;

    @Parameter(name = "product")
    private String product;

    @Parameter(name = "side")
    private Side side;

    @Parameter(name = "amount")
    private Integer amount;

    @Parameter(name = "price")
    private Integer price;

    @Parameter(name = "broker")
    private String broker;

    @Parameter(name = "client")
    private String client;

    public Integer getOrderId() {
        return orderId;
    }

    public Integer getModifiedOrderId() {
        return modifiedOrderId;
    }

    public Integer getCancelledOrderId() {
        return cancelledOrderId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public MessageType getType() {
        return type;
    }

    public String getProduct() {
        return product;
    }

    public Side getSide() {
        return side;
    }

    public Integer getAmount() {
        return amount;
    }

    public Integer getPrice() {
        return price;
    }

    public String getBroker() {
        return broker;
    }

    public String getClient() {
        return client;
    }
}
