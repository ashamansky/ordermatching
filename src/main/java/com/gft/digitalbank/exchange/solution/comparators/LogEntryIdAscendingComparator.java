package com.gft.digitalbank.exchange.solution.comparators;

import com.gft.digitalbank.exchange.solution.models.LogEntry;

import java.util.Comparator;

public class LogEntryIdAscendingComparator implements Comparator<LogEntry> {
    @Override
    public int compare(LogEntry o1, LogEntry o2) {
        return o1.getId() - o2.getId();
    }
}
