package com.gft.digitalbank.exchange.solution.consumers;

import com.gft.digitalbank.exchange.solution.converters.EventMessageConverter;
import com.gft.digitalbank.exchange.solution.events.Event;
import com.gft.digitalbank.exchange.solution.events.ShutdownEvent;
import com.gft.digitalbank.exchange.solution.listeners.OrderProcessingListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;

import javax.jms.JMSException;
import javax.jms.TextMessage;

public class BrokerConsumer extends SimpleJmsListenerEndpoint {

    private OrderProcessingListener listener;
    private EventMessageConverter eventMessageConverter;
    private static Logger logger = LoggerFactory.getLogger(BrokerConsumer.class);

    public BrokerConsumer(String destination, OrderProcessingListener orderProcessingListener, EventMessageConverter eventMessageConverter) {
        super();
        this.setId(destination);
        this.setDestination(destination);
        this.listener = orderProcessingListener;
        this.eventMessageConverter=eventMessageConverter;
        this.setMessageListener(message -> {
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                try {
                    logger.trace("Message: {}", textMessage.getText());
                    Event event = this.eventMessageConverter.convert(textMessage.getText());
                    synchronized (orderProcessingListener) {
                        if (event instanceof ShutdownEvent) {
                            listener.handle(event);
                        } else {
                            logger.trace("Expected: {}, received: {}", orderProcessingListener.getNextTimeStamp(), event.getTimeStamp());
                            while (orderProcessingListener.getNextTimeStamp() != event.getTimeStamp()) {
                                try {
                                    logger.trace("Waiting {}", Thread.currentThread().getName());
                                    orderProcessingListener.wait();
                                    logger.trace("Awoken {}", Thread.currentThread().getName());
                                } catch (InterruptedException e) {
                                    logger.error("Sleep interrupted", e);
                                    Thread.currentThread().interrupt();
                                }
                            }
                            listener.handle(event);
                        }
                    }

                } catch (JMSException e) {
                    logger.error("BrokerConsumer: {}", e);
                }
            } else {
                logger.error("Jms message has not a text type: {}", message.getClass().getName());
            }
        });
    }



}
