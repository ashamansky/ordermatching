package com.gft.digitalbank.exchange.solution.matching.impl;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.comparators.*;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMapper;
import com.gft.digitalbank.exchange.solution.matching.sequence.OrderIDCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class OrderBookMapper {
    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderIDCreator orderIDCreator;

    private OrderEntryTimestampAscendingComparator sortByTimestampAscending;
    private OrderEntryAscendingPriceComparator sortByPriceAscending;
    private OrderEntryDescendingPriceComparator sortByPriceDescending;

    OrderBookMapper(){
        this.sortByTimestampAscending = new OrderEntryTimestampAscendingComparator();
        this.sortByPriceAscending = new OrderEntryAscendingPriceComparator();
        this.sortByPriceDescending = new OrderEntryDescendingPriceComparator();
    }

    public OrderBook convert(OrderRegistry registry) {
        this.orderIDCreator.reset();

        return OrderBook.builder()
                .product(registry.getProduct())
                .buyEntries(registry.getBuyEntries().entries().stream()
                        .sorted(sortByPriceDescending.thenComparing(sortByTimestampAscending))
                        .map(orderEntry -> this.orderMapper.assignID(registry.getProduct(), Side.BUY, orderEntry))
                        .collect(Collectors.toList())
                )
                .sellEntries(registry.getSellEntries().entries().stream()
                        .sorted(sortByPriceAscending.thenComparing(sortByTimestampAscending))
                        .map(orderEntry -> this.orderMapper.assignID(registry.getProduct(), Side.SELL, orderEntry))
                        .collect(Collectors.toList())
                )
                .build();
    }

}
