package com.gft.digitalbank.exchange.solution.matching.sequence;

import com.koloboke.collect.map.hash.HashObjIntMap;
import com.koloboke.collect.map.hash.HashObjIntMaps;
import org.springframework.stereotype.Component;


@Component
public class TransactionIDCreator {
    private HashObjIntMap<String> transactionIds;

    TransactionIDCreator() {
        init();
    }

    public int getNewIdFor(String product) {
        return getAndIncrement(product);
    }

    public void reset() {
        init();
    }

    private void init() {
        this.transactionIds = HashObjIntMaps.newMutableMap();
    }

    private int getAndIncrement(String product) {
        if (!this.transactionIds.containsKey(product)) {
            return addProduct(product);
        } else {
            return increaseTransactionIdOf(product);
        }
    }

    private int increaseTransactionIdOf(String product) {
        int lastTransactionId = this.transactionIds.getInt(product);
        this.transactionIds.put(product, ++lastTransactionId);
        return lastTransactionId;
    }

    private int addProduct(String product) {
            this.transactionIds.put(product, 1);
            return 1;
    }
}
