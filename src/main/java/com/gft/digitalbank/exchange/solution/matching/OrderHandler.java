package com.gft.digitalbank.exchange.solution.matching;

import com.gft.digitalbank.exchange.solution.events.Event;

public interface OrderHandler<T extends Event> {
    void handle(T event);
}
