package com.gft.digitalbank.exchange.solution.matching;

public interface OrderRegistry {
    OrderList getBuyEntries();
    OrderList getSellEntries();
    String getProduct();
    Boolean isEmpty();
}
