package com.gft.digitalbank.exchange.solution.repository.impl;

import com.gft.digitalbank.exchange.solution.comparators.LogEntryIdAscendingComparator;
import com.gft.digitalbank.exchange.solution.models.LogEntry;
import com.gft.digitalbank.exchange.solution.repository.TransactionRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;


@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

    private List<LogEntry> logEntries = new CopyOnWriteArrayList<>();

    private LogEntryIdAscendingComparator sortByEntryIdAscending;

    public TransactionRepositoryImpl() {
        this.sortByEntryIdAscending = new LogEntryIdAscendingComparator();
    }


    @Override
    public List<LogEntry> findAll() {
        return logEntries.parallelStream()
                .sorted(sortByEntryIdAscending).collect(Collectors.toList());
    }

    @Override
    public List<LogEntry> add(LogEntry logEntry) {
        return logEntries.add(logEntry) ? logEntries : null;
    }

    @Override
    public void removeAll() {
        logEntries.clear();
    }
}
