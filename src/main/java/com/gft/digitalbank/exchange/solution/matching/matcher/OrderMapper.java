package com.gft.digitalbank.exchange.solution.matching.matcher;

import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.matching.sequence.OrderIDCreator;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper {

    @Autowired
    private OrderIDCreator orderIdCreator;

    public com.gft.digitalbank.exchange.model.OrderEntry convert(OrderEntry entry) {
        return com.gft.digitalbank.exchange.model.OrderEntry.builder()
                .amount(entry.getAmount())
                .broker(entry.getBroker())
                .client(entry.getClient())
                .price(entry.getPrice())
                .id(entry.getId())
                .build();
    }

    private OrderEntry.OrderEntryBuilder partialConvert(PositionOrder positionOrder) {
        return OrderEntry.builder()
                .price(positionOrder.getDetails().getPrice())
                .timestamp(positionOrder.getTimestamp())
                .amount(positionOrder.getDetails().getAmount())
                .client(positionOrder.getClient())
                .broker(positionOrder.getBroker());
    }

    public OrderEntry convert(PositionOrder positionOrder) {
        return partialConvert(positionOrder)
            .id(this.orderIdCreator.getNewIdFor(positionOrder))
            .build();
    }

    public OrderEntry convertKeepId(PositionOrder positionOrder) {
        return partialConvert(positionOrder)
                .id(positionOrder.getId())
                .build();
    }

    public com.gft.digitalbank.exchange.model.OrderEntry assignID(String product, Side side,
                                                                  OrderEntry orderEntry) {
        return com.gft.digitalbank.exchange.model.OrderEntry.builder()
                .amount(orderEntry.getAmount())
                .broker(orderEntry.getBroker())
                .client(orderEntry.getClient())
                .price(orderEntry.getPrice())
                .id(this.orderIdCreator.getNewIdFor(product, side))
                .build();
    }
}
