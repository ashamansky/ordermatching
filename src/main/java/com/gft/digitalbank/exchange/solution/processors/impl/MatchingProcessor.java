package com.gft.digitalbank.exchange.solution.processors.impl;

import com.gft.digitalbank.exchange.solution.events.*;
import com.gft.digitalbank.exchange.solution.matching.handlers.*;
import com.gft.digitalbank.exchange.solution.processors.EventProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MatchingProcessor implements EventProcessor {

    @Autowired
    private BuyOrderHandler buyOrderHandler;

    @Autowired
    private SellOrderHandler sellOrderHandler;

    @Autowired
    private CancellationOrderHandler cancellationOrderHandler;

    @Autowired
    private ShutdownOrderHandler shutdownOrderHandler;

    @Autowired
    private ModificationOrderHandler modificationOrderHandler;


    @Override
    public void handle(BuyEvent event) {
        buyOrderHandler.handle(event);
    }

    @Override
    public void handle(SellEvent event) {
        sellOrderHandler.handle(event);
    }

    @Override
    public void handle(ModificationEvent event) {
        this.modificationOrderHandler.handle(event);
    }

    @Override
    public void handle(CancellationEvent event) {
        this.cancellationOrderHandler.handle(event);
    }

    @Override
    public void handle(ShutdownEvent event) {
        shutdownOrderHandler.handle(event);
    }

}
