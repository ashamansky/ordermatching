package com.gft.digitalbank.exchange.solution.converters;

public final class BrokerMessageConstants {

    public static final String MESSAGE_TYPE = "messageType";

    public static final String INVALID_JSON_FORMAT = "Invalid json format!";
    public static final String BROKER_MESSAGE_NOT_SUPPORTED = "Broker message is not supported!";
    public static final String MESSAGE_TYPE_NOT_SUPPORTED = "MessageType is not supported!";
    public static final String SIDE_NOT_SUPPORTED = "Side is not supported!";

    private BrokerMessageConstants(){}
}
