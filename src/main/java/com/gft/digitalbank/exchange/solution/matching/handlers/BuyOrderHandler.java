package com.gft.digitalbank.exchange.solution.matching.handlers;

import com.gft.digitalbank.exchange.solution.events.BuyEvent;
import com.gft.digitalbank.exchange.solution.matching.*;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMapper;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMatcher;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class BuyOrderHandler implements OrderHandler<BuyEvent> {

    @Autowired
    private OrderBookService orderBookService;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private TransactionService transactionService;

    public Collection<OrderEntry> handle(OrderEntry buyOrder, OrderList buyEntries, OrderList sellEntries) {
        final OrderMatcher matcher = new OrderMatcher(sellEntries::findByPriceUpTo);
        return matcher.handle(buyOrder, sellEntries, buyEntries);
    }

    @Override
    public void handle(BuyEvent event) {
        OrderRegistry registry = orderBookService.getBook(event.getPositionOrder().getProduct());
            OrderEntry buyOrderEntry = orderMapper.convertKeepId(event.getPositionOrder());
            Collection<OrderEntry> matches = handle(buyOrderEntry, registry.getBuyEntries(), registry.getSellEntries());
            matches.forEach(match -> {
                OrderEntry soldEntry = buyOrderEntry.toBuilder().price(match.getPrice()).build();
                transactionService.log(event.getPositionOrder().getProduct(), soldEntry, match);
            });
    }
}
