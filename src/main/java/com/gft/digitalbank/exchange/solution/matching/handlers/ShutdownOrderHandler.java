package com.gft.digitalbank.exchange.solution.matching.handlers;

import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.model.SolutionResult;
import com.gft.digitalbank.exchange.solution.events.ShutdownEvent;
import com.gft.digitalbank.exchange.solution.matching.OrderBookService;
import com.gft.digitalbank.exchange.solution.matching.OrderHandler;
import com.gft.digitalbank.exchange.solution.matching.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ShutdownOrderHandler implements OrderHandler<ShutdownEvent> {

    @Autowired
    private OrderBookService orderBookService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private ProcessingListener processingListener;

    @Override
    public void handle(ShutdownEvent event) {
        SolutionResult result = new SolutionResult(transactionService.build(), orderBookService.build());
        orderBookService.clear();
        processingListener.processingDone(result);
    }
}
