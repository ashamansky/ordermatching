package com.gft.digitalbank.exchange.solution.models;

import lombok.Value;


@Value
public class LogEntry {
    int id;
    String product;
    OrderEntry buyEntry;
    OrderEntry sellEntry;
}
