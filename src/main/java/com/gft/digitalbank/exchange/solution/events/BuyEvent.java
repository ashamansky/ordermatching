package com.gft.digitalbank.exchange.solution.events;

import com.gft.digitalbank.exchange.solution.processors.EventProcessor;
import lombok.Value;


@Value
public class BuyEvent implements Event, PositionOrderEvent {
    com.gft.digitalbank.exchange.model.orders.PositionOrder positionOrder;

    private long timeStamp;

    @Override
    public void dispatchTo(EventProcessor processor) {
        processor.handle(this);
    }

}
