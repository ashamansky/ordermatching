package com.gft.digitalbank.exchange.solution.listeners;

import com.gft.digitalbank.exchange.solution.events.Event;
import com.gft.digitalbank.exchange.solution.events.ShutdownEvent;
import com.gft.digitalbank.exchange.solution.repository.BrokerRepository;
import com.gft.digitalbank.exchange.solution.routers.EventRouter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Component
@Slf4j
public class OrderProcessingListener {

    @Autowired
    private EventRouter eventRouter;

    @Autowired
    private BrokerRepository brokerRepository;

    private AtomicInteger brokerCount = new AtomicInteger(0);

    private AtomicLong orderTimestamp = new AtomicLong(1);

    public long getNextTimeStamp() {
        return this.orderTimestamp.get();
    }

    public synchronized void handle(Event event) {
        log.trace("Route event: {}", event);
        if (event instanceof ShutdownEvent) {
            log.trace("brokerCount: {} from instance: {}", brokerCount.get(), this);
            if(brokerCount.decrementAndGet() == 0) {
                eventRouter.route(new ShutdownEvent(null, 1));
                orderTimestamp = new AtomicLong(1);
            }
        } else {
                eventRouter.route(event);
                orderTimestamp.incrementAndGet();
                this.notifyAll();
        }
    }


    @PostConstruct
    public void init() {
        brokerCount = new AtomicInteger(brokerRepository.getBrokerCount());
        log.trace("init brokerRepository {} and timestamp:{}", brokerCount.get(), getNextTimeStamp());
    }
}
