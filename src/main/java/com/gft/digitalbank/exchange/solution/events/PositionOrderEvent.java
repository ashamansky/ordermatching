package com.gft.digitalbank.exchange.solution.events;

public interface PositionOrderEvent {
    com.gft.digitalbank.exchange.model.orders.PositionOrder getPositionOrder();
}
