package com.gft.digitalbank.exchange.solution.repository;

import com.gft.digitalbank.exchange.solution.models.LogEntry;

import java.util.List;

/**
 * Created by alexander on 25.07.16.
 */
public interface TransactionRepository {
    List<LogEntry> findAll();
    List<LogEntry> add(LogEntry logEntry);
    void removeAll();
}
