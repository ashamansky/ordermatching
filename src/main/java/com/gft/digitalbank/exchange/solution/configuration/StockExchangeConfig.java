package com.gft.digitalbank.exchange.solution.configuration;

import com.gft.digitalbank.exchange.solution.consumers.BrokerConsumer;
import com.gft.digitalbank.exchange.solution.converters.EventMessageConverter;
import com.gft.digitalbank.exchange.solution.listeners.OrderProcessingListener;
import com.gft.digitalbank.exchange.solution.processors.impl.MatchingProcessor;
import com.gft.digitalbank.exchange.solution.repository.BrokerRepository;
import com.gft.digitalbank.exchange.solution.routers.EventRouter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.jms.connection.SingleConnectionFactory;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;
import java.util.stream.Collectors;


@Configuration
@ComponentScan(basePackages = {"com.gft.digitalbank.exchange.solution.*"})
@SuppressWarnings("SpringJavaAutowiringInspection")
@EnableJms
public class StockExchangeConfig {

    private static final String DEFAULT_CONCURRENCY = "1";
    private static final String CONNECTION_FACTORY = "ConnectionFactory";

    @Autowired
    private MatchingProcessor matchingProcessor;

    @Autowired
    private EventMessageConverter eventMessageConverter;

    @Autowired
    private BrokerRepository brokerRepository;

    @Autowired
    private OrderProcessingListener orderProcessingListener;

    @Bean
    public JmsListenerContainerFactory jmsListenerContainerFactory() throws NamingException {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setConcurrency(getConcurrency());
        return factory;
    }


    @Bean
    public ConnectionFactory connectionFactory() throws NamingException {
        Context context = new InitialContext();
        ActiveMQConnectionFactory factory = (ActiveMQConnectionFactory) context.lookup(CONNECTION_FACTORY);
        factory.setExclusiveConsumer(true);
        return new SingleConnectionFactory(factory);
    }

    @Bean
    public JmsListenerConfigurer jmsListenerConfigurer() {
        List<String> destinations = brokerRepository.getDestinations();
        if (destinations != null && !destinations.isEmpty()) {
            List<? extends SimpleJmsListenerEndpoint> consumers = destinations.stream()
                    .map(destination -> new BrokerConsumer(destination, orderProcessingListener, eventMessageConverter))
                    .collect(Collectors.toList());
            return jmsListenerEndpointRegistrar -> consumers.forEach(jmsListenerEndpointRegistrar::registerEndpoint);
        } else {
            return null;
        }
    }

    private String getConcurrency() {
        List<String> destinations = brokerRepository.getDestinations();
        return destinations != null ? String.valueOf(destinations.size()) : DEFAULT_CONCURRENCY;
    }


    @Bean
    public EventRouter eventRouter() {
        EventRouter router = new EventRouter();
        router.register(matchingProcessor);
        return router;
    }

}
