package com.gft.digitalbank.exchange.solution.repository;

import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;

import java.util.Collection;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Created by alexander on 25.07.16.
 */
public interface OrderRepository {
    void initRegistries(Map<String, OrderRegistry> registryMap);

    OrderRegistry findOrderBookByProduct(String product);
    OrderRegistry findOrderBookByBrokerAndId(String broker, int orderId);
    Predicate<OrderRegistry> byBrokerAndId(String broker, int orderId);
    void removeByBrokerAndId(String broker, int id);
    Collection<OrderRegistry> findAllOrders();
    void removeAll();
}
