package com.gft.digitalbank.exchange.solution.events;

import com.gft.digitalbank.exchange.solution.processors.EventProcessor;
import lombok.Value;

@Value
public class CancellationEvent implements Event {
    com.gft.digitalbank.exchange.model.orders.CancellationOrder order;

    private long timeStamp;

    public void dispatchTo(EventProcessor processor) {
        processor.handle(this);
    }
}
