package com.gft.digitalbank.exchange.solution.comparators;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;

import java.util.Comparator;

public class OrderEntryTimestampAscendingComparator implements Comparator<OrderEntry> {
    @Override
    public int compare(OrderEntry o1, OrderEntry o2) {
        return Math.toIntExact(o1.getTimestamp() - o2.getTimestamp());
    }
}
