package com.gft.digitalbank.exchange.solution.comparators;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;

import java.util.Comparator;

public class OrderEntryAscendingPriceComparator implements Comparator<OrderEntry> {
    @Override
    public int compare(OrderEntry o1, OrderEntry o2) {
        return o1.getPrice() - o2.getPrice();
    }
}
