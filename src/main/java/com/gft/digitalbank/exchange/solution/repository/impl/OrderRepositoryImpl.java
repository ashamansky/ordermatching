package com.gft.digitalbank.exchange.solution.repository.impl;

import com.gft.digitalbank.exchange.solution.matching.OrderList;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.matching.impl.OrderRegistryImpl;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.repository.OrderRepository;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.util.StringUtils.isEmpty;

@Repository
public class OrderRepositoryImpl implements OrderRepository {
    private Map<String, OrderRegistry> registries = new ConcurrentHashMap<>();



    @Override
    public void initRegistries(Map<String, OrderRegistry> registryMap) {
        this.registries =registryMap;
    }

    @Override
    public OrderRegistry findOrderBookByProduct(String product) {
        if (!registries.containsKey(product)) {
            registries.putIfAbsent(product, new OrderRegistryImpl(product));
        }
        return registries.get(product);
    }

    @Override
    public OrderRegistry findOrderBookByBrokerAndId(String broker, int orderId) {
        Optional<OrderRegistry> first = registries.values().parallelStream().filter(byBrokerAndId(broker, orderId)).findFirst();
        if (first.isPresent())
            return first.get();
        return null;
    }

    @Override
    public Predicate<OrderRegistry> byBrokerAndId(String broker, int orderId) {
        return orderRegistry ->
                isFoundInBuyEntries(orderRegistry, broker, orderId)
                        || isFoundInSellEntries(orderRegistry, broker, orderId);
    }

    @Override
    public Collection<OrderRegistry> findAllOrders() {
        return registries.values();
    }

    @Override
    public void removeAll() {
        registries.clear();
    }

    @Override
    public void removeByBrokerAndId(String broker, int id) {
        List<OrderList> orderLists = registries.values().parallelStream()
                .flatMap(registry -> Stream.of(registry.getBuyEntries(), registry.getSellEntries()))
                .collect(Collectors.toList());

        for(OrderList orderList:orderLists){
            orderList.removeByBrokerAndId(broker,id);
        }
    }

    private boolean isFoundInBuyEntries(OrderRegistry orderRegistry, String broker, int orderId) {
        OrderEntry orderEntry = orderRegistry.getBuyEntries().findById(orderId);
        return !isEmpty(orderEntry) && orderEntry.getBroker().equals(broker);
    }

    private boolean isFoundInSellEntries(OrderRegistry orderRegistry, String broker, int orderId) {
        OrderEntry orderEntry = orderRegistry.getSellEntries().findById(orderId);
        return !isEmpty(orderEntry) && orderEntry.getBroker().equals(broker);
    }
}

