package com.gft.digitalbank.exchange.solution.repository.impl;

import com.gft.digitalbank.exchange.solution.repository.BrokerRepository;

import java.util.ArrayList;
import java.util.List;

public class BrokerRepositoryImpl implements BrokerRepository{

    private List<String> destinations = new ArrayList<>();

    public BrokerRepositoryImpl(List<String> destinations) {
        this.destinations = destinations;
    }

    @Override
    public List<String> getDestinations() {
        return this.destinations;
    }

    @Override
    public int getBrokerCount() {
        return destinations.size();
    }
}
