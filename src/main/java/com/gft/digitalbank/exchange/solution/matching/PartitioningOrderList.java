package com.gft.digitalbank.exchange.solution.matching;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PartitioningOrderList implements OrderList {
    private static final Comparator<OrderEntry> TIMESTAMP_COMPARATOR = (lhs, rhs) -> Long.compare(lhs.getTimestamp(), rhs.getTimestamp());
    private static final Comparator<OrderEntry> ENTRY_COMPARATOR = (lhs, rhs) -> Long.compare(lhs.getPrice(), rhs.getPrice());
    private ArrayList<OrderEntry> entriesLo = new ArrayList<>();
    private ArrayList<OrderEntry> entriesHi = new ArrayList<>();

    private OrderEntry pivot() {
        return entriesLo.get(0);
    }

    @Override
    public void add(final OrderEntry order) {
        if(entriesLo.isEmpty()) {
            entriesLo.add(order);
        } else if (order.getPrice() <= pivot().getPrice()) {
            entriesLo.add(order);
        } else {
            entriesHi.add(order);
        }
    }

    @Override
    public void remove(OrderEntry order) {
        if(entriesLo.isEmpty()) {
            return;
        }
        entriesLo.remove(order);
        entriesHi.remove(order);
        ensureLoIsPopulated();
    }

    private void ensureLoIsPopulated() {
        if(entriesLo.isEmpty() && !entriesHi.isEmpty()) {
            swapLoHi();
            updatePivot();
        }
    }

    private void updatePivot() {
        Collections.swap(entriesLo, 0, findPivot());
    }

    final void swapLoHi() {
        final ArrayList<OrderEntry> tmp = entriesHi;
        entriesHi = entriesLo;
        entriesLo = tmp;

    }

    private int findPivot() {
        int pivotNew = 0;
        for(int i = 1; i < entriesLo.size(); ++i) {
            if(ENTRY_COMPARATOR.compare(entriesLo.get(i), entriesLo.get(pivotNew)) > 0) {
                pivotNew = i;
            }
        }
        return pivotNew;
    }

    @Override
    public OrderEntry findById(int id) {
        return Stream.concat(entriesLo.stream(), entriesHi.stream()).filter(o -> id == o.getId())
                .collect(Collectors.minBy(TIMESTAMP_COMPARATOR))
                .orElse(null);
    }

    @Override
    public void removeByBrokerAndId(String broker, int id) {
        Arrays.asList(entriesLo, entriesHi)
            .forEach(entries -> entries.stream()
                .filter(e -> broker.equals(e.getBroker()) && id == e.getId())
                .findFirst()
                .ifPresent(this::remove));
    }

    @Override
    public OrderEntry findByMaxPrice() {
        if(entriesLo.isEmpty()) {
            return null;
        }

        final ArrayList<OrderEntry> entries = entriesHi.isEmpty()
                ? entriesLo
                : entriesHi;

        return entries.stream()
                    .collect(Collectors.maxBy(ENTRY_COMPARATOR.thenComparing(TIMESTAMP_COMPARATOR.reversed())))
                    .orElse(null);
    }

    @Override
    public OrderEntry findByMinPrice() {
        if(entriesLo.isEmpty()) {
            return null;
        }

        return entriesLo.stream()
                .collect(Collectors.minBy(ENTRY_COMPARATOR.thenComparing(TIMESTAMP_COMPARATOR)))
                .orElse(null);
    }

    @Override
    public Collection<OrderEntry> findByPriceDownTo(int price) {
        partition(price-1);

        ArrayList<OrderEntry> result = entriesHi;
        result.sort(ENTRY_COMPARATOR.reversed().thenComparing(TIMESTAMP_COMPARATOR));
        entriesHi = new ArrayList<>();

        ensureLoIsPopulated();
        return result;
    }

    @Override
    public Collection<OrderEntry> findByPriceUpTo(int price) {
        partition(price);

        ArrayList<OrderEntry> result = entriesLo;
        result.sort(ENTRY_COMPARATOR.thenComparing(TIMESTAMP_COMPARATOR));
        entriesLo = new ArrayList<>();

        ensureLoIsPopulated();
        return result;
    }

    // Hoare partition
    private void partition(final int price) {
        int i = 0;
        int j = 0;
        final int sizeLo = entriesLo.size();
        final int sizeHi = entriesHi.size();
        while(i < sizeLo && j < sizeHi) {
            // Find element that does not belong in low entries
            for(;i < sizeLo; ++i) {
                final int loSwapCandidate = entriesLo.get(i).getPrice();
                if(loSwapCandidate > price) {
                    break;
                }
            }

            // Find element that does not belong in hi entries
            for(;j < sizeHi; ++j) {
                final int hiSwapCandidate = entriesHi.get(j).getPrice();
                if(hiSwapCandidate <= price) {
                    break;
                }
            }

            // Swap candidates if they were found in both lists
            if(i < sizeLo && j < sizeHi) {
                final OrderEntry tooHigh = entriesLo.get(i);
                entriesLo.set(i, entriesHi.get(j));
                entriesHi.set(j, tooHigh);
            }
        }

        partitionLoTail(price, i, sizeLo);
        partitionHiTail(price, j, sizeHi);
    }

    private void partitionHiTail(final int price, final int from, final int to) {
        for(int l = to -1; l >= from; --l) {
            if(entriesHi.get(l).getPrice() <= price) {
                entriesLo.add(entriesHi.remove(l));
            }
        }
    }

    private void partitionLoTail(final int price, final int from, final int to) {
        for(int k = to -1; k >= from; --k) {
            if(entriesLo.get(k).getPrice() > price) {
                entriesHi.add(entriesLo.remove(k));
            }
        }
    }

    @Override
    public Collection<OrderEntry> entries() {
        ImmutableCollection.Builder<OrderEntry> builder = ImmutableList.builder();
        return builder.addAll(entriesLo)
                .addAll(entriesHi)
                .build();
    }

    @Override
    public boolean isEmpty() {
        return entriesHi.isEmpty() && entriesLo.isEmpty();
    }
}
