package com.gft.digitalbank.exchange.solution.matching.impl;

import com.gft.digitalbank.exchange.solution.comparators.OrderEntryAscendingPriceComparator;
import com.gft.digitalbank.exchange.solution.comparators.OrderEntryDescendingPriceComparator;
import com.gft.digitalbank.exchange.solution.matching.OrderList;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.google.common.collect.ArrayListMultimap;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

public class HashMultimapOrderList implements OrderList {
    private static final Comparator<? super OrderEntry> TIMESTAMP_COMPARATOR = (lhs, rhs) -> Long.compare(lhs.getTimestamp(), rhs.getTimestamp());
    private final ArrayListMultimap<Integer, OrderEntry> entries = ArrayListMultimap.create();



    @Override
    public void add(OrderEntry order) {
        entries.put(order.getPrice(), order);
    }

    @Override
    public synchronized void remove(OrderEntry order) {
        entries.remove(order.getPrice(), order);
    }

    @Override
    public OrderEntry findById(int id) {
        return entries.values().stream().filter(o -> id == o.getId())
                .collect(Collectors.minBy(TIMESTAMP_COMPARATOR))
                .orElse(null);
    }

    @Override
    public void removeByBrokerAndId(String broker, int id) {
        entries.entries().stream()
                .filter(e -> broker.equals(e.getValue().getBroker()) && id == e.getValue().getId())
                .findFirst()
                .ifPresent(e -> entries.remove(e.getKey(), e.getValue()));
    }

    @Override
    public OrderEntry findByMaxPrice() {
        if(entries.isEmpty())
            return null;

        final int maxPrice = entries.keys().stream()
                .collect(Collectors.maxBy(Integer::compare))
                .get();

        return minTimestamp(maxPrice);
    }

    @Override
    public OrderEntry findByMinPrice() {
        if(entries.isEmpty())
            return null;

        final int minPrice = entries.keys().stream()
                .collect(Collectors.minBy(Integer::compare))
                .get();

        return minTimestamp(minPrice);
    }

    @Override
    public Collection<OrderEntry> findByPriceDownTo(int price) {
        if(entries.isEmpty())
            return Collections.emptyList();

        Collection<OrderEntry> result = entries.keySet().stream().filter(p -> p >= price)
                .sorted(Comparator.reverseOrder())
                .map(entries::get)
                .flatMap(Collection::stream)
                .sorted(new OrderEntryDescendingPriceComparator())
                .collect(Collectors.toList());

        result.forEach(e -> entries.remove(e.getPrice(), e));

        return result;
    }

    @Override
    public Collection<OrderEntry> findByPriceUpTo(int price) {
        if(entries.isEmpty())
            return Collections.emptyList();

        Collection<OrderEntry> result = entries.keySet().stream().filter(p -> p <= price)
                .sorted()
                .map(entries::get)
                .flatMap(Collection::stream)
                .sorted(new OrderEntryAscendingPriceComparator())
                .collect(Collectors.toList());

        result.forEach(e -> entries.remove(e.getPrice(), e));

        return result;
    }

    private OrderEntry minTimestamp(int minPrice) {
        return entries.get(minPrice).stream()
                .collect(Collectors.minBy(TIMESTAMP_COMPARATOR))
                .orElse(null);
    }

    @Override
    public Collection<OrderEntry> entries() {
        return entries.values();
    }

    @Override
    public boolean isEmpty() {
        return entries.isEmpty();
    }
}
