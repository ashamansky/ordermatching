package com.gft.digitalbank.exchange.solution.repository;


import java.util.List;

public interface BrokerRepository {
    List<String> getDestinations();
    int getBrokerCount();
}
