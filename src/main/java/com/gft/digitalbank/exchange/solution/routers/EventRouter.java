package com.gft.digitalbank.exchange.solution.routers;

import com.gft.digitalbank.exchange.solution.events.Event;
import com.gft.digitalbank.exchange.solution.processors.EventProcessor;

import java.util.HashSet;
import java.util.Set;

public class EventRouter {
    private Set<EventProcessor> processors = new HashSet<>();

    public void route(Event event) {
        processors.forEach(event::dispatchTo);
    }

    public void register(EventProcessor processor) {
        processors.add(processor);
    }

    public void unregisterProcessor(EventProcessor processor) {
        processors.remove(processor);
    }

    public void unregisterAllProcessors() {
        processors.clear();
    }

    public Set<EventProcessor> getProcessors() {
        return processors;
    }
}
