package com.gft.digitalbank.exchange.solution.matching.impl;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import com.gft.digitalbank.exchange.solution.matching.sequence.TransactionIDCreator;
import com.gft.digitalbank.exchange.solution.matching.TransactionMapper;
import com.gft.digitalbank.exchange.solution.matching.TransactionService;
import com.gft.digitalbank.exchange.solution.models.LogEntry;
import com.gft.digitalbank.exchange.solution.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class LazyTransactionService implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionMapper transactionMapper;

    @Autowired
    private TransactionIDCreator transactionIDCreator;

    @Override
    public void log(String product, OrderEntry buyEntry, OrderEntry sellEntry) {
        transactionRepository.add(new LogEntry(transactionIDCreator.getNewIdFor(product), product, buyEntry, sellEntry));
    }

    @Override
    public Set<Transaction> build() {
        Set<Transaction> transactions = Collections.synchronizedSet(new HashSet<>());
        List<LogEntry> logEntries = transactionRepository.findAll();
        logEntries
                .stream()
                .map(transactionMapper::createTransaction)
                .collect(Collectors.toCollection(() -> transactions));
        clear();
        return transactions;
    }

    @Override
    public void clear() {
        this.transactionRepository.removeAll();
        this.transactionIDCreator.reset();
    }
}
