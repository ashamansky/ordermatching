package com.gft.digitalbank.exchange.solution.matching.impl;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.solution.matching.OrderBookService;
import com.gft.digitalbank.exchange.solution.matching.sequence.OrderIDCreator;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Profile("!test")
public class OrderBookServiceImpl implements OrderBookService {
    @Autowired
    private OrderBookMapper mapper;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderIDCreator orderIDCreator;

    @Override
    public OrderRegistry getBook(String product) {
        return orderRepository.findOrderBookByProduct(product);
    }


    @Override
    public Set<OrderBook> build() {
        Set<OrderBook> orderBooks = new HashSet<>();

        List<OrderRegistry> orderRegistries = orderRepository.findAllOrders().parallelStream()
                .filter(orderRegistry -> !orderRegistry.isEmpty())
                .collect(Collectors.toList());

        for (OrderRegistry orderRegistry: orderRegistries) {
            orderBooks.add(mapper.convert(orderRegistry));
        }

        clear();
        return orderBooks;
    }

    @Override
    public OrderRegistry findBookByBrokerAndId(String broker, int orderId) {
        return orderRepository.findOrderBookByBrokerAndId(broker, orderId);
    }


    @Override
    public void cancelByBrokerAndId(String broker, int orderId) {
        orderRepository.removeByBrokerAndId(broker, orderId);
    }

    @Override
    public void clear() {
        this.orderRepository.removeAll();
        this.orderIDCreator.reset();
    }
}
