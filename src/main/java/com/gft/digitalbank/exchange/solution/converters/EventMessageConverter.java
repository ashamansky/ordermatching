package com.gft.digitalbank.exchange.solution.converters;

import com.gft.digitalbank.exchange.model.orders.*;
import com.gft.digitalbank.exchange.solution.events.*;
import com.gft.digitalbank.exchange.solution.events.CancellationEvent;
import com.gft.digitalbank.exchange.solution.events.ModificationEvent;
import com.gft.digitalbank.exchange.solution.exceptions.InvalidBrokerMessageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.gft.digitalbank.exchange.solution.converters.BrokerMessageConstants.*;
import static org.springframework.util.StringUtils.isEmpty;


@Component
public class EventMessageConverter {

    @Autowired
    private BrokerMessageConverter brokerMessageConverter;

    public Event convert(String jsonText) {
        try {
            BrokerMessage brokerMessage = brokerMessageConverter.convert(jsonText);
            return convert(brokerMessage);
        } catch (Exception e) {
            throw new InvalidBrokerMessageException(e);
        }
    }

    public Event convert(BrokerMessage brokerMessage) {
        Event event = convertMessageToEvent(brokerMessage);
        if (isEmpty(event))
            throw new InvalidBrokerMessageException(BROKER_MESSAGE_NOT_SUPPORTED);
        return event;
    }

    private Event convertMessageToEvent(BrokerMessage brokerMessage) {
        Event event;
        switch (brokerMessage.getMessageType()) {
            case ORDER:
                event = convertToPositionOrderEvent(brokerMessage);
                break;
            case MODIFICATION:
                event = new ModificationEvent(
                        (ModificationOrder) brokerMessage, brokerMessage.getTimestamp());
                break;
            case CANCEL:
                event = new CancellationEvent(
                        (CancellationOrder) brokerMessage, brokerMessage.getTimestamp());
                break;
            case SHUTDOWN_NOTIFICATION:
                event = new ShutdownEvent((ShutdownNotification) brokerMessage, brokerMessage.getTimestamp());
                break;
            default:
                throw new InvalidBrokerMessageException(MESSAGE_TYPE_NOT_SUPPORTED);
        }
        return event;
    }

    private Event convertToPositionOrderEvent(BrokerMessage brokerMessage) {
        Event event;
        Side side = ((PositionOrder) brokerMessage).getSide();
        switch (side) {
            case BUY:
                event = new BuyEvent((PositionOrder) brokerMessage, brokerMessage.getTimestamp());
                break;
            case SELL:
                event = new SellEvent((PositionOrder) brokerMessage, brokerMessage.getTimestamp());
                break;
            default:
                throw new InvalidBrokerMessageException(SIDE_NOT_SUPPORTED);
        }
        return event;
    }
}