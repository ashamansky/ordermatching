package com.gft.digitalbank.exchange.solution.converters;

import com.gft.digitalbank.exchange.model.orders.*;
import com.gft.digitalbank.exchange.solution.exceptions.InvalidBrokerMessageException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

import static com.gft.digitalbank.exchange.solution.converters.BrokerMessageConstants.*;
import static org.springframework.util.StringUtils.isEmpty;

@Component
@Scope(value = "prototype")
public class BrokerMessageConverter {

    private static final Gson gson = new Gson();

    public BrokerMessage convert(String jsonText) {
        BrokerMessage entity;
        try {
            JsonElement jsonElement = gson.fromJson(jsonText, JsonElement.class);
            entity = gson.fromJson(jsonElement, new BrokerMessageType(jsonElement).getType());
            if (isEmpty(entity))
                throw new InvalidBrokerMessageException(INVALID_JSON_FORMAT);
        } catch (Exception e) {
            throw new InvalidBrokerMessageException(e);
        }
        return entity;
    }

    private class BrokerMessageType {
        Type type;

        BrokerMessageType(JsonElement jsonElement) {
            setType(jsonElement);
        }

        private void setType(JsonElement jsonElement) {
            MessageType messageType =
                    MessageType.valueOf(jsonElement.getAsJsonObject().get(MESSAGE_TYPE).getAsString());

            switch (messageType) {
                case ORDER:
                    type = PositionOrder.class;
                    break;
                case MODIFICATION:
                    type = ModificationOrder.class;
                    break;
                case CANCEL:
                    type = CancellationOrder.class;
                    break;
                case SHUTDOWN_NOTIFICATION:
                    type = ShutdownNotification.class;
                    break;
                default:
                    throw new InvalidBrokerMessageException(MESSAGE_TYPE_NOT_SUPPORTED);
            }
        }

        Type getType() {
            return this.type;
        }
    }
}

