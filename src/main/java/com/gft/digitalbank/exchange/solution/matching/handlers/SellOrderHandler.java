package com.gft.digitalbank.exchange.solution.matching.handlers;

import com.gft.digitalbank.exchange.solution.events.SellEvent;
import com.gft.digitalbank.exchange.solution.matching.*;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMapper;
import com.gft.digitalbank.exchange.solution.matching.matcher.OrderMatcher;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class SellOrderHandler implements OrderHandler<SellEvent> {

    @Autowired
    private OrderBookService orderBookService;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private TransactionService transactionService;

    public Collection<OrderEntry> handle(OrderEntry sellOrder, OrderList buyEntries, OrderList sellEntries) {
        final OrderMatcher matcher = new OrderMatcher(buyEntries::findByPriceDownTo);
        return matcher.handle(sellOrder, buyEntries, sellEntries);
    }

    @Override
    public void handle(SellEvent event) {
        OrderRegistry registry = orderBookService.getBook(event.getPositionOrder().getProduct());
        OrderEntry orderEntry = orderMapper.convertKeepId(event.getPositionOrder());
        Collection<OrderEntry> matches = handle(orderEntry, registry.getBuyEntries(), registry.getSellEntries());
        matches.forEach(match -> transactionService.log(event.getPositionOrder().getProduct(), match, orderEntry));
    }
}
