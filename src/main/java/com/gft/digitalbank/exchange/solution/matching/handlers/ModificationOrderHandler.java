package com.gft.digitalbank.exchange.solution.matching.handlers;

import com.gft.digitalbank.exchange.model.orders.ModificationOrder;
import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.events.BuyEvent;
import com.gft.digitalbank.exchange.solution.events.ModificationEvent;
import com.gft.digitalbank.exchange.solution.events.SellEvent;
import com.gft.digitalbank.exchange.solution.matching.*;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.springframework.util.StringUtils.isEmpty;


@Component
public class ModificationOrderHandler implements OrderHandler<ModificationEvent> {

    @Autowired
    private OrderBookService orderBookService;

    @Autowired
    private BuyOrderHandler buyOrderHandler;

    @Autowired
    private SellOrderHandler sellOrderHandler;

    @Override
    public void handle(ModificationEvent event) {
        handleOrder(event);
    }

    private void handleOrder(ModificationEvent event) {
        OrderRegistry registry = this.orderBookService.findBookByBrokerAndId(
                event.getOrder().getBroker(), event.getOrder().getModifiedOrderId());
        if (!isEmpty(registry)) {
            OrderEntry orderEntry = registry.getBuyEntries().findById(event.getOrder().getModifiedOrderId());
            if (!isEmpty(orderEntry) && isSameBroker(orderEntry.getBroker(), event.getOrder().getBroker())) {
                OrderList buyEntries = registry.getBuyEntries();
                buyEntries.remove(orderEntry);
                if(isRemoved(orderEntry.getId(), buyEntries)) {
                    PositionOrder positionOrder = modifyEvent(orderEntry, event.getOrder(), Side.BUY, registry.getProduct());
                    BuyEvent buyEvent = new BuyEvent(positionOrder, event.getTimeStamp());
                    buyOrderHandler.handle(buyEvent);
                }
            } else {
                orderEntry = registry.getSellEntries().findById(event.getOrder().getModifiedOrderId());
                if (!isEmpty(orderEntry) && isSameBroker(orderEntry.getBroker(), event.getOrder().getBroker())) {
                    OrderList sellEntries = registry.getSellEntries();
                    sellEntries.remove(orderEntry);
                    if(isRemoved(orderEntry.getId(), sellEntries)) {
                        PositionOrder positionOrder = modifyEvent(orderEntry, event.getOrder(), Side.SELL, registry.getProduct());
                        SellEvent sellEvent = new SellEvent(positionOrder, event.getTimeStamp());
                        sellOrderHandler.handle(sellEvent);
                    }
                }
            }
        }
    }

    private boolean isRemoved(int id, OrderList entries) {
        return isEmpty(entries.findById(id));
    }

    private boolean isSameBroker(String modifier, String creator) {
        return modifier.equals(creator);
    }

    private PositionOrder modifyEvent(OrderEntry orderEntry, ModificationOrder modificationOrder, Side side, String product) {
        return PositionOrder.builder()
                .broker(orderEntry.getBroker())
                .client(orderEntry.getClient())
                .id(orderEntry.getId())
                .timestamp(modificationOrder.getTimestamp())
                .details(modificationOrder.getDetails())
                .side(side)
                .product(product)
                .build();
    }
}
