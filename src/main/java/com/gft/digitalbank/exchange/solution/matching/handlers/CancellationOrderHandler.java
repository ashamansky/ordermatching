package com.gft.digitalbank.exchange.solution.matching.handlers;

import com.gft.digitalbank.exchange.solution.events.CancellationEvent;
import com.gft.digitalbank.exchange.solution.matching.OrderBookService;
import com.gft.digitalbank.exchange.solution.matching.OrderHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class CancellationOrderHandler implements OrderHandler<CancellationEvent> {

    @Autowired
    private OrderBookService orderBookService;

    @Override
    public void handle(CancellationEvent event) {
        this.orderBookService.cancelByBrokerAndId(event.getOrder().getBroker(), event.getOrder().getCancelledOrderId());
    }
}
