package com.gft.digitalbank.exchange.solution.models;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class OrderEntry {
    int id;
    int price;
    int amount;
    long timestamp;
    String client;
    String broker;
}
