package com.gft.digitalbank.exchange.solution.matching.impl;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.matching.TransactionMapper;
import com.gft.digitalbank.exchange.solution.models.LogEntry;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapperImpl implements TransactionMapper {

    @Override
    public Transaction createTransaction(LogEntry logEntry) {
        return Transaction.builder()
                .id(logEntry.getId()) // Not sure about this
                .amount(Math.min(logEntry.getBuyEntry().getAmount(), logEntry.getSellEntry().getAmount()))
                .price(logEntry.getBuyEntry().getPrice())
                .brokerBuy(logEntry.getBuyEntry().getBroker())
                .brokerSell(logEntry.getSellEntry().getBroker())
                .clientBuy(logEntry.getBuyEntry().getClient())
                .clientSell(logEntry.getSellEntry().getClient())
                .product(logEntry.getProduct())
                .build();
    }
}


