package com.gft.digitalbank.exchange.solution.matching.sequence;

import com.gft.digitalbank.exchange.model.orders.PositionOrder;
import com.gft.digitalbank.exchange.model.orders.Side;
import com.gft.digitalbank.exchange.solution.exceptions.InvalidBrokerMessageException;
import com.koloboke.collect.map.hash.HashObjObjMap;
import com.koloboke.collect.map.hash.HashObjObjMaps;
import org.springframework.stereotype.Component;

import static com.gft.digitalbank.exchange.solution.converters.BrokerMessageConstants.SIDE_NOT_SUPPORTED;

@Component
public class OrderIDCreator {
    private HashObjObjMap<String, LastOrderID> lastOrderIds;

    public OrderIDCreator() {
        init();
    }

    private void init() {
        this.lastOrderIds = HashObjObjMaps.newMutableMap();
    }

    public void reset() {
        init();
    }

    public int getNewIdFor(PositionOrder positionOrder) {
        if(lastOrderIds == null)
            init();
        return getAndIncrement(positionOrder.getProduct(), positionOrder.getSide());
    }

    public int getNewIdFor(String product, Side side) {
        return getAndIncrement(product, side);
    }

    private int getAndIncrement(String product, Side side) {
        if (!this.lastOrderIds.containsKey(product)) {
            return addProductWithSide(product, side);
        } else {
            switch (side) {
                case BUY:
                    return increaseBuyEntriesIdOf(product);
                case SELL:
                    return increaseSellEntriesIdOf(product);
                default:
                    throw new InvalidBrokerMessageException(SIDE_NOT_SUPPORTED);
            }
        }

    }

    private int increaseBuyEntriesIdOf(String product) {
        LastOrderID lastOrderID = this.lastOrderIds.get(product);
        lastOrderID.increaseBuyEntriesId();
        this.lastOrderIds.put(product, lastOrderID);
        return lastOrderID.getBuyEntriesId();
    }

    private int increaseSellEntriesIdOf(String product) {
        LastOrderID lastOrderID = this.lastOrderIds.get(product);
        lastOrderID.increaseSellEntriesId();
        this.lastOrderIds.put(product, lastOrderID);
        return lastOrderID.getSellEntriesId();
    }

    private int addProductWithSide(String product, Side side) {
        LastOrderID lastOrderID = new LastOrderID();
        switch (side) {
            case SELL: lastOrderID.increaseSellEntriesId();
                break;
            case BUY: lastOrderID.increaseBuyEntriesId();
                break;
            default: throw new InvalidBrokerMessageException(SIDE_NOT_SUPPORTED);
        }
        this.lastOrderIds.put(product, lastOrderID);
        return 1;
    }

    private class LastOrderID {
        private int buyEntriesId = 0;
        private int sellEntriesId = 0;

        int getBuyEntriesId() {
            return buyEntriesId;
        }

        void increaseBuyEntriesId() {
            ++buyEntriesId;
        }

        int getSellEntriesId() {
            return sellEntriesId;
        }

        void increaseSellEntriesId() {
            ++sellEntriesId;
        }
    }
}
