package com.gft.digitalbank.exchange.solution.events;

import com.gft.digitalbank.exchange.model.orders.ShutdownNotification;
import com.gft.digitalbank.exchange.solution.processors.EventProcessor;
import lombok.Value;

@Value
public class ShutdownEvent implements Event {
    ShutdownNotification notification;

    private long timeStamp;

    @Override
    public void dispatchTo(EventProcessor processor) {
        processor.handle(this);
    }
}
