package com.gft.digitalbank.exchange.solution.exceptions;

public class InvalidBrokerMessageException extends RuntimeException{

    public InvalidBrokerMessageException(String message){
        super(message);
    }

    public InvalidBrokerMessageException(Exception e) {
        super(e);
    }
}
