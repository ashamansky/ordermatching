package com.gft.digitalbank.exchange.solution.matching;

import com.gft.digitalbank.exchange.solution.models.OrderEntry;

import java.util.Collection;

public interface OrderList {
    void add(OrderEntry order);
    void remove(OrderEntry order);
    void removeByBrokerAndId(String broker, int id);
    OrderEntry findById(int id);
    OrderEntry findByMaxPrice();
    OrderEntry findByMinPrice();
    Collection<OrderEntry> findByPriceDownTo(int price);
    Collection<OrderEntry> findByPriceUpTo(int price);
    Collection<OrderEntry> entries();
    boolean isEmpty();
}
