package com.gft.digitalbank.exchange.solution.matching;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;

import java.util.Set;

public interface TransactionService {
    void log(String product, OrderEntry buyEntry, OrderEntry sellEntry);
    Set<Transaction> build();
    void clear();
}
