package com.gft.digitalbank.exchange.solution.matching.matcher;

import com.gft.digitalbank.exchange.solution.matching.OrderList;
import com.gft.digitalbank.exchange.solution.models.OrderEntry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;

public class OrderMatcher {
    private final Function<Integer, Collection<OrderEntry>> candidateProducer;

    public OrderMatcher(Function<Integer, Collection<OrderEntry>> candidateProducer) {
        this.candidateProducer = candidateProducer;
    }

    public Collection<OrderEntry> handle(OrderEntry incomingEntry, OrderList outstandingOrders, OrderList remainder) {
        Collection<OrderEntry> candidates = candidateProducer.apply(incomingEntry.getPrice());

        if(candidates.isEmpty()) {
            remainder.add(incomingEntry);
        } else {
            Collection<OrderEntry> matches = new ArrayList<>(candidates.size());
            int amountLeft = incomingEntry.getAmount();
            Iterator<OrderEntry> it = candidates.iterator();
            if(it.hasNext()) {
                OrderEntry candidate = it.next();
                while(amountLeft - candidate.getAmount() > 0 && it.hasNext()) { // Full match
                    amountLeft -= candidate.getAmount();
                    matches.add(candidate);
                    candidate = it.next();
                }

                amountLeft -= candidate.getAmount();

                if(amountLeft == 0) {
                    matches.add(candidate);
                } else if(amountLeft < 0) { // Partial match
                    matches.add(candidate.toBuilder()
                            .amount(candidate.getAmount() + amountLeft)
                            .build());

                    outstandingOrders.add(candidate.toBuilder().amount(-amountLeft).build());
                } else if(amountLeft > 0) {
                    matches.add(candidate);
                    remainder.add(incomingEntry.toBuilder().amount(amountLeft).build());
                }

                while(it.hasNext()) {
                    outstandingOrders.add(it.next());
                }
            }

            return matches;
        }
        return Collections.emptyList();
    }
}
