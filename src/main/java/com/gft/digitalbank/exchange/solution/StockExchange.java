package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.Exchange;
import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.solution.configuration.StockExchangeConfig;
import com.gft.digitalbank.exchange.solution.repository.BrokerRepository;
import com.gft.digitalbank.exchange.solution.repository.impl.BrokerRepositoryImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jms.config.JmsListenerEndpointRegistry;

import java.util.List;

/**
 * Your solution must implement the {@link Exchange} interface.
 */
@Slf4j
public class StockExchange implements Exchange {
    private AnnotationConfigApplicationContext parent;

    @Override
    public void register(ProcessingListener listener) {
        ProcessingListener listenerProxy = solutionResult -> {
            log.info("Closing context {}", parent);
            listener.processingDone(solutionResult);

            JmsListenerEndpointRegistry registry = parent.getBean(JmsListenerEndpointRegistry.class);
            registry.stop();

            parent.close();
            parent = null;
        };
        parent.getBeanFactory().registerResolvableDependency(ProcessingListener.class, listenerProxy);
    }

    public StockExchange() {
        parent = new AnnotationConfigApplicationContext();
        parent.register(StockExchangeConfig.class);
    }

    @Override
    public void setDestinations(List<String> list) {
        parent.getBeanFactory().registerResolvableDependency(BrokerRepository.class, new BrokerRepositoryImpl(list));
        parent.refresh();
    }

    @Override
    public void start() {
        parent.start();
    }
}
