package com.gft.digitalbank.exchange.solution.matching;

import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.models.LogEntry;


public interface TransactionMapper {
    Transaction createTransaction(LogEntry logEntry);
}
