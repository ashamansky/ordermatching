package com.gft.digitalbank.exchange.solution.processors;

import com.gft.digitalbank.exchange.solution.events.*;


public interface EventProcessor {
    void handle(BuyEvent event);
    void handle(SellEvent event);
    void handle(ModificationEvent event);
    void handle(CancellationEvent event);
    void handle(ShutdownEvent event);
}
