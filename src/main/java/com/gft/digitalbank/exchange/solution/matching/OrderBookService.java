package com.gft.digitalbank.exchange.solution.matching;

import com.gft.digitalbank.exchange.model.OrderBook;

import java.util.Set;

public interface OrderBookService {
    OrderRegistry getBook(String product);
    Set<OrderBook> build();
    OrderRegistry findBookByBrokerAndId(String broker, int id);
    void cancelByBrokerAndId(String broker, int id);
    void clear();
}
