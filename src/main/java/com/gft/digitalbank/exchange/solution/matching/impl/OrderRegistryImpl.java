package com.gft.digitalbank.exchange.solution.matching.impl;

import com.gft.digitalbank.exchange.solution.matching.OrderList;
import com.gft.digitalbank.exchange.solution.matching.OrderRegistry;
import com.gft.digitalbank.exchange.solution.matching.PartitioningOrderList;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OrderRegistryImpl implements OrderRegistry {
    private final String product;
    private final OrderList buyEntries = new PartitioningOrderList();
    private final OrderList sellEntries = new PartitioningOrderList();

    @Override
    public Boolean isEmpty() {
        return buyEntries.isEmpty() && sellEntries.isEmpty();
    }
}
