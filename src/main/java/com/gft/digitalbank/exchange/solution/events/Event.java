package com.gft.digitalbank.exchange.solution.events;

import com.gft.digitalbank.exchange.solution.processors.EventProcessor;


public interface Event {
    void dispatchTo(EventProcessor processor);
    long getTimeStamp();

}
